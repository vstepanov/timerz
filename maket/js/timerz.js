(function(window) {
  var _timers = {}, // таблица таймеров
  _id = 0, // последний id
  _ticks = [], // список id тикающих таймеров
  _time = +new Date()/1000|0; // текущий timestamp
  
  window.timerz = function(expiry, tick) {
    var id = ++_id;
    this.id = id;
    this.timeleft = {d: null, h: null, m: null, s: null};
    _timers[id] = {id: id, object: this, expiry: null, tick: null, timeleft: null};
    if(typeof expiry != 'undefined')
      _setExpiry(id, expiry);
    if(typeof tick != 'undefined')
      _setTick(id, tick);
  }
  
  timerz.prototype.onTick = function(tick) {
    _setTick(this.id, tick);
  }
  
  timerz.prototype.setExpiry = function(expiry) {
    _setExpiry(this.id, expiry);
  }
  
  timerz.prototype.getExpiry = function() {
    return _timers[id].expiry;
  }
  
  function _setExpiry(id, expiry) {
    var _expiry = null;
    if(typeof expiry == 'object' && expiry instanceof Date)
      _expiry = expiry.getTime()/1000|0; // |0 - отбрасывает дробную часть
    else
      _expiry = expiry;
    _timers[id].expiry = _expiry;
    _checkTick(id);
  }
  
  function _setTick(id, tick) {
    if(typeof tick == 'function') {
      _timers[id].tick = tick;
    }
    _checkTick(id);
  }
  
  function _checkTick(id) {
    var timer = _timers[id];
    if(_ticks.indexOf(id) < 0 && typeof timer.tick == 'function' && timer.expiry) {
      _ticks.push(id);
      _tickOnTimer(id);
    }
  }
  
  function _tickOnTimer(id) {
    var timer = _timers[id];
    timer.timeleft = timer.expiry - _time;
    if(timer.timeleft <= 0) {
      timer.timeleft = 0;
      _ticks.splice(_ticks.indexOf(timer.id), 1); // удаляем id-таймера из списка тикающих
    }
    timer.object.timeleft.d = timer.timeleft/86400|0;
    var t = timer.object.timeleft.d*86400;
    timer.object.timeleft.h = (timer.timeleft - t)/3600|0;
    t += timer.object.timeleft.h*3600;
    timer.object.timeleft.m = (timer.timeleft - t)/60|0;
    timer.object.timeleft.s = timer.timeleft - t - timer.object.timeleft.m*60;
    timer.tick.call(timer.object);
  }

  function _tick() {
    _time = +new Date()/1000|0;
    for(var i=0;i<_ticks.length;i++)
      _tickOnTimer(_ticks[i]);
    setTimeout(_tick, 1000);
  }
  
  _tick();
  
})(window);