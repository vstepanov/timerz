<?php

return array(
  'pdoClass' => 'NestedPDO',
  'connectionString' => 'mysql:host=localhost;dbname=timerz',
  'emulatePrepare' => true,
  'username' => 'root',
  'password' => 'root',
  'charset' => 'utf8',
  'tablePrefix' => '',
  'enableProfiling' => true,
  'enableParamLogging' => true,
  /*'schemaCachingDuration'=>3600,*/
);