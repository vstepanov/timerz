<?php

return array(
	'class'=>'CLogRouter',
	'routes'=>array(
	  array(
      'class'=>'CProfileLogRoute',
      'report'=>'summary',
      // Показывает время выполнения каждого отмеченного блока кода.
      // Значение "report" также можно указать как "callstack".
    ),
		array(
			'class'=>'CWebLogRoute',
			'categories'=>'system.db.*',
		),
		array(
      'class'=>'CFileLogRoute',
      'levels'=>'error, warning',
    ),
	),
);