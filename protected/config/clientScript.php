<?php

return array(
  'class'=>'ext.minScript.components.ExtMinScript',
  'minScriptLmCache' => '86400',
  'minScriptDebug' => YII_DEBUG,
  'coreScriptPosition' => CClientScript::POS_END,
  'minScriptCacheId' => 'File',
  'packages' => array(
    'js' => array(
      'baseUrl' => 'js',
      'js' => array(
        'jquery-1.10.2.min.js',
        'bootstrap.min.js',
        'date.format.js',
        'jquery.ui.widget.js',
        'jquery.iframe-transport.js',
        'jquery.fileupload.js',
        'jquery.transit.min.js',
        'cookie.js',
        'lib.js',
        'main.js',
        'timerzcore.js',
        'timerzrender.js',
      ),
    ),
    'css' => array(
      'baseUrl' => 'css',
      'css' => array(
        'bootstrap.min.css',
        'styles.css',
        'jquery.fileupload-ui.css',
      ),
    ),
  ),
);