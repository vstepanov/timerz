<?php

return array(
  '/' => 'main/homepage',
  
  /* Security */
  'signin' => 'security/signin',
  'signup' => 'security/signup',
  'signout' => 'security/signout',
  'sign/<userId:\d+>' => 'test/signin',
  'mail/<view:\w+>' => 'test/mail',
  'restore-password' => 'security/restorePassword',
  'restore-password-sended' => 'security/restorePasswordSended',
  
  /* Account */
  'account' => 'account/index',
  'account/change-password' => 'account/changePassword',
  'account/change-nick' => 'account/changeNick',
  'account/change-email' => 'account/changeEmail',
  
  /* Timer */
  'timer/strtotime' => 'timer/strtotime',
  'timer/uploadCover' => 'timer/uploadCover',
  'timer/getMessage' => 'timer/getMessage',
  '@<timerIdentifier:\w+>' => 'timer/show',
  '@<timerIdentifier:\w+>/edit' => 'timer/edit',
  '@<timerIdentifier:\w+>/watch' => array('timer/watching', 'defaultParams' => array('action' => 'watching')),
  '@<timerIdentifier:\w+>/stopWatching' => array('timer/watching', 'defaultParams' => array('action' => 'stopWatching')),
  '@<timerIdentifier:\w+>/watchers' => 'timer/watchers',
  /*'<userIdentifier:\w+>/@<timerIdentifier:\w+>' => array('timer/show'),*/
  'create' => 'timer/create',
  '<type:(last|popular|soon|ended)>' => 'timer/list',
  
  /* User */
  '<userIdentifier:\w+>/<type:(last|popular|soon|ended|watching)>' => array('user/show'),// all|last|popular|soon|ended|watching
  '<userIdentifier:\w+>' => array('user/show', 'defaultParams' => array('type' => 'soon')),
  
  /* Default */
  '<controller:\w+>/<id:\d+>'=>'<controller>/view',
  '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
  '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
);