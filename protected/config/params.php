<?php

return array(
	// this is used in contact page
	'adminEmail'            => 'webmaster@timerz.org',
	'notificationEmail'     => 'notification@timerz.org',
	'Imagick'               => false,
	'coverPath'             => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cover',
	'coverBaseUrl'          => '/cover',
	'tmpCoverPath'          => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cover'.DIRECTORY_SEPARATOR.'_tmp',
	'tmpCoverBaseUrl'       => '/cover/_tmp',
	'maxUploadCoverSize'    => 5242880, // in bytes
	'timersPerPage'         => 10,
	'thumbnailCoverWidth'   => 640,
	'thumbnailCoverHeight'  => 480,
	'coverWidth'            => 2048,
	'coverHeight'           => 1536,
	'stopnicks'             => require_once(dirname(__FILE__) . '/stopnicks.php'),
);