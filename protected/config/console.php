<?php

require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'base.php');

return array(
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'timerz',
	'language' => 'ru',

	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.yii-mail.YiiMailMessage',
	),

	'components'=>array(
		'db' => require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/db.php'),
    'mail' => require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/mail.php'),
	),
	'params' => require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/params.php'),
);