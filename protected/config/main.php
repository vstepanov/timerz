<?php

require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'base.php');

defined('SCREEN_WIDTH') or define('SCREEN_WIDTH', isset($_COOKIE['screen_width']) && $_COOKIE['screen_width'] > 0 ? $_COOKIE['screen_width'] : null);

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'timerz',
	'language' => 'ru',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.yii-mail.YiiMailMessage',
	),
	
	'controllerMap'=>array(
    'min'=>array(
      'class'=>'ext.minScript.controllers.ExtMinScriptController',
    ),
  ),
	
	// application components
	'components'=>array(
	  
		'user'=>array(
		  'class' => 'WebUser',
      'allowAutoLogin'=>true,
      'loginUrl' => array('security/signin'),
		),
		
		'urlManager'=>array(
		  'showScriptName' => false,
			'urlFormat'=>'path',
			'rules'=> require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/routing.php'),
		),
		
		'cache' => require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/cache.php'),
		
		'cacheFile' => require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/cacheFile.php'),
		
		'db' => require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/db.php'),
		
		'clientScript' => require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/clientScript.php'),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
      'errorAction'=>'main/error',
    ),
    
    'mail' => require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/mail.php'),
    
		'log' => require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/log.php'),
	),
  
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'/params.php'),
);