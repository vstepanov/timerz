<?php

setlocale(LC_ALL, 'ru_RU','rus_RUS','Russian');

date_default_timezone_set('UTC');

mb_regex_encoding('UTF-8');
mb_internal_encoding('UTF-8');