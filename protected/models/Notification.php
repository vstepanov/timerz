<?php

class Notification
{
  
  /************************************************** PUBLIC STATIC **************************************************/
  
  /**
   * @return bool
   */
  public static function newUser(User $user)
  {
    return self::send($user->email, 'Добро пожаловать', 'newUser', array('user' => $user));
  }
  
  /**
   * @return bool
   */
  public static function newTimer(Timer $timer)
  {
    return true;
    //return self::send($timer->user->email, 'Вы добавили таймер: ' . CHtml::encode($timer->caption), 'newTimer', array('timer' => $timer));
  }
  
  /**
   * @return bool
   */
  public static function restorePassword(User $user)
  {
    return self::send($user->email, 'Восстановление пароля', 'restorePassword', array('user' => $user));
  }
  
  /************************************************** PRIVATE STATIC **************************************************/
  
  private static function send($email, $subject, $view, $params)
  {
    if(!$email)
      return false;
    $message = new YiiMailMessage;
    $message->view = $view;
    $message->setBody($params, 'text/html'); 
    $message->subject = $subject;
    $message->addTo($email);
    $message->from = array(Yii::app()->params['notificationEmail'] => 'Timerz');
    return Yii::app()->mail->send($message);
  }
  
}