<?php

/**
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $nickname
 * @property string $salt
 * @property string $created_at
 */
class User extends Model
{
  public $current_password;
  public $new_password;
  public $password_again;
  
  private static $_instance = null; // current logged in user
  private static $_hashids = null;
  
  /************************************************** PUBLIC STATIC **************************************************/
  
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
  
  public static function decodeHash($hash)
  {
    $ret = self::_hashids()->decrypt($hash);
    return isset($ret[0]) ? $ret[0] : null;
  }
  
  public static function encodeHash($id)
  {
    return  self::_hashids()->encrypt((integer) $id);
  }
  
  /**
   * @param integer $userId
   * @return User
   */
  public static function retrieve($userId)
  {
    return User::model()->findByPk($userId);
  }
  
  public static function retrieveByHash($hash)
	{
  	return self::model()->findByPk(self::decodeHash($hash));
	}
	
	public static function retrieveByIdentifier($identifier)
	{
	  if(self::decodeHash($identifier))
	    return self::retrieveByHash($identifier);
  	else
  	  return self::retrieveByNickname($identifier);
	}
	
	public static function retrieveByLogin($login)
	{
	  $login = strtolower($login);
  	$user = User::model()->find('LOWER(email)=?',array($login));
  	if(!$user)
  	  $user = User::model()->find('LOWER(nickname)=?',array($login));
  	 return $user;
	}
	
	public static function retrieveByNickname($nickname)
	{
	  if(in_array($nickname, Yii::app()->params['stopnicks']))
	    return false;
  	return User::model()->findByAttributes(array('nickname' => $nickname));
	}
	
	/**
   * @param string $email
   * @return User
   */
  public static function retrieveByEmail($email)
	{
  	return User::model()->findByAttributes(array('email' => $email));
	}
	
  /**
   * @param string $email
   * @return User
   */
  public static function createByEmail($email)
  {
    try
    {
      $user = new User('insert');
      $user->setAttribute('email', $email);
      $user->setRestorePasswordHash(86400*30, false);
      if($user->save(false, array('email', 'restore_hash', 'restore_valid')))
      {
        Notification::newUser($user);
        Yii::app()->user->loginAsUser($user);
        return $user;
      }
      else
        return false;
    }
    catch(Exception $e)
    {
      return false;
    }
  }
    
  /**
   * Retrieve current User object if user is logged in
   *
   * @return User || false
   */
  public static function instance()
  {
    if(is_null(self::$_instance))
    {
      if(Yii::app()->user->isGuest)
        self::$_instance = false;
      else
        self::$_instance = self::retrieve(Yii::app()->user->id);
    }
    return self::$_instance;
  }
  
  public static function identifier($id, $nickname = null)
  {
    return $nickname ? $nickname : self::encodeHash($id);
  }
  
  public static function url($userIdentifier, $action = 'show', $params = null, $absolute = false)
	{
	  if(is_array($action))
	  {
  	  $params = $action;
  	  $action = 'show';
	  }
	  elseif(is_bool($action))
	  {
  	  $absolute = $action;
  	  $action = 'show';
	  }
    if(is_null($params))
      $params = array();
    if(!is_array($params))
    {
      $absolute = $params;
      $params = array();
    }
    $params['userIdentifier'] = $userIdentifier;
    return $absolute ? Yii::app()->createAbsoluteUrl('user/'.$action, $params) : Yii::app()->createUrl('user/'.$action, $params);
	}
  
  /************************************************** PUBLIC **************************************************/
  
  public function getHash()
	{
  	return self::encodeHash($this->getPrimaryKey());
	}
	
	public function tableName()
	{
		return 'user';
	}
  
	public function rules()
	{
		return array(
		  array('current_password', 'currentPasswordValidation', 'on' => 'changePassword'),
		  array('nickname', 'nicknameValidation'),
			array('email, password, nickname, password_again, new_password', 'required'),
			array('password_again', 'compare', 'compareAttribute' => 'new_password'),
			array('email', 'email'),
			array('email, nickname', 'unique'),
			array('email', 'length', 'max' => 45),
			array('nickname', 'length', 'max' => 20, 'min' => 4),
			array('nickname', 'match', 'pattern' => '/^[a-zA-Z0-9]{4,20}$/', 'message' => 'Допускаются только английские буквы и цифры'),
		);
	}
	
	public function currentPasswordValidation($attribute, $params)
	{
  	if($this->hasPassword() && !$this->validatePassword($this->current_password))
  	  $this->addError('current_password', 'Текущий пароль указан неверно');
	}
	
	public function nicknameValidation($attribute, $params)
	{
	  $nickname = $this->getNickname();
  	if(!is_null(User::decodeHash($nickname)) || !is_null(Timer::decodeHash($nickname)) || in_array($nickname, Yii::app()->params['stopnicks']))
  	  $this->addError('nickname', 'Этот псевдоним недоступен, попробуйте выбрать другой');
	}
	
	public function attributeLabels()
	{
		return array(
		  'email' => 'Email',
		  'password' => Yii::t('user', 'Password'),
		  'new_password' => Yii::t('user', 'New password'),
		  'nickname' => Yii::t('user', 'Nickname'),
		  'current_password' => Yii::t('user', 'Current password'),
		  'password_again' => Yii::t('user', 'New password again'),
		);
	}
	
	public function relations()
	{
  	return array(
  	  'timers' => array(self::HAS_MANY, 'Timer', 'created_by'),
  	);
	}
	
	public function hashPassword($password)
  {
    return CPasswordHelper::hashPassword($password);
  }
  
  public function setPassword($password)
  {
    $this->setAttribute('password',  $this->hashPassword($password));
  }
  
  public function validatePassword($password)
  {
    return CPasswordHelper::verifyPassword($password, $this->password);
  }
  
  public function getNickname()
  {
    return $this->getAttribute('nickname');
  }
  
  public function isMe()
  {
    return !Yii::app()->user->isGuest && Yii::app()->user->getId() == $this->getPrimaryKey();
  }
	
	public function getLink($action = 'show', $params = null, $absolute = false)
	{
    return self::url($this->getIdentifier(), $action, $params, $absolute);
	}
  
  /**
   * Проверяет может ли пользователь войти самостоятельно через форму или через соц.сети
   * Возвращает true, если у пользователя есть пароль или подключена хоть одна соц.сеть
   *
   * @return boolean
   */
  public function canSelfLogin()
  {
    return $this->hasPassword();
  }
  
  public function hasPassword()
  {
    return (bool) $this->getAttribute('password');
  }
  
  public function hasEmail()
  {
    return (bool) $this->getAttribute('email');
  }
  
  public function hasNickname()
  {
    return (bool) $this->getAttribute('nickname');
  }
  
  public function getIdentifier()
  {
    return self::identifier($this->getPrimaryKey(), $this->getNickname());
  }
  
  public function setRestorePasswordHash($validDuration = 86400, $save = true)
  {
    $this->setAttributes(array(
      'restore_hash' => md5(time().$this->salt),
      'restore_valid' => date('Y-m-d H:i:s', time() + $validDuration)
    ), false);
    if($save && $this->save(false, array('restore_hash', 'restore_valid')))
      return Notification::restorePassword($this);
    return !$save ? true : false;
  }
  
  public function restorePasswordValid($hash)
  {
    return $this->restore_hash && $this->restore_hash == $hash && $this->restore_valid && strtotime($this->restore_valid) >= time();
  }
  
  public function resetPassword()
  {
    $this->setAttributes(array(
      'password' => null,
      'restore_hash' => null,
      'restore_valid' => null,
    ), false);
    return $this->save(false, array('password', 'restore_hash', 'restore_valid'));
  }
  
  public function getRestorePasswordLink($absolute = true)
  {
    if($absolute)
      return Yii::app()->createAbsoluteUrl('security/restorePassword', array('user' => $this->getHash(), 'key' => $this->restore_hash));
    else
      return Yii::app()->createUrl('security/restorePassword', array('user' => $this->getHash(), 'key' => $this->restore_hash));
  }
  
  /************************************************** PROTECTED **************************************************/
  
  protected function beforeSave()
  {
    if(parent::beforeSave())
    {      
      if($this->isNewRecord)
        $this->salt = uniqid('', true);
      if($this->getNickname() != Yii::app()->user->name)
        Yii::app()->user->name = $this->getNickname();
      return true;
    }
  }
  
  /************************************************** PRIVATE STATIC **************************************************/
  
  private static function _hashids()
  {
    if(is_null(self::$_hashids))
      self::$_hashids = new Hashids\Hashids('2398fg2PFg39p7WGfF3hi20foevjgieowugf', 6);
    return self::$_hashids;
  }
  
}