<?php

/**
 * @property integer  $id
 * @property string   $template_name
 * @property string   $recipients
 * @property string   $params
 * @property string   $status
 * @property string   $error_message
 * @property integer  $max_attempts
 * @property integer  $attempts
 * @property string   $last_attempt_time
 * @property string   $created_at
 */
class EmailQueue extends Model
{

  /************************************************** PUBLIC STATIC **************************************************/
    
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
  
  public static function push($template_name, $email, $params = null)
  {
    
  }
    
  /************************************************** PUBLIC **************************************************/
    
	public function tableName()
	{
		return 'email_queue';
	}
  
  /************************************************** PROTECTED **************************************************/
    
}