<?php

class SigninForm extends CFormModel
{
  public $login;
	public $password;
	
	private $_identity;
	
	public function rules()
  {
    return array(
      array('login, password', 'required'),
      array('login', 'authenticate'),
    );
  }
  
  public function attributeLabels()
	{
		return array(
		  'login' => Yii::t('security', 'Email or nickname'),
		  'password' => Yii::t('user', 'Password'),
		);
	}
  
  public function authenticate($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity = new UserIdentity($this->login, $this->password);
			if($this->_identity->authenticate())
			  Yii::app()->user->login($this->_identity);
			else
				$this->addError('password', Yii::t('security', 'Wrong login and/or password'));
		}
	}
}