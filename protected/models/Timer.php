<?php

/**
 * @property integer $id
 * @property integer $expires
 * @property string $caption
 * @property string $message
 * @property User $created_by
 * @property string $created_at
 * @property integer $viewcount
 * @property string $cover_url
 * @property boolean $is_comments
 * @property string $cover
 * @property integer $privacy
 * @property integer $watchcount
 * @property string $local_alias
 * @property string $global_alias
 */

class Timer extends Model
{
  public $utcoffset;
  public $expires_value;
  public $email;

  const THUMBNAIL = true;
  
  const PRIVACY_ALL       = 0;
  const PRIVACY_PROFILE   = 100;
  const PRIVACY_LINK      = 200;
  const PRIVACY_ONLYME    = 255;
  
  private static $_hashids = null;
  private $_coverTmp;
  
  /************************************************** PUBLIC STATIC **************************************************/
  
  public static function decodeHash($hash)
  {
    $ret = self::_hashids()->decrypt($hash);
    return isset($ret[0]) ? $ret[0] : null;
  }
  
  public static function encodeHash($id)
  {
    return  self::_hashids()->encrypt((integer) $id);
  }
  
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
  
  public static function retrieve($id)
	{
  	return Timer::model()->findByPk($id);
	}
	
	public static function retrieveByHash($hash)
	{
  	return self::model()->findByPk(self::decodeHash($hash));
	}
	
	public static function retrieveByIdentifier($timerIdentifier)
	{
	  $parsedIdentifier = self::parseIdentifier($timerIdentifier);
	  if(isset($parsedIdentifier['hash'])) // by hash
	    return self::retrieveByHash($parsedIdentifier['hash']);
	  elseif(isset($parsedIdentifier['alias']))
	  {
  	  if(in_array($parsedIdentifier['alias'], Yii::app()->params['stopnicks']))
  	    return false;
  	  if(isset($parsedIdentifier['userIdentifier'])) // by local_alias
  	  {
    	  $user = User::retrieveByIdentifier($userIdentifier);
    	  if(!$user)
    	    return false;
      	$timer = Timer::model()->findByAttributes(array('created_by' => $user->getPrimaryKey(), 'local_alias' => $parsedIdentifier['alias']));
      	if(!$timer)
      	  return false;
      	 $timer->author = $user;
        return $timer;
  	  }
  	  else // by global_alias
        return Timer::model()->findByAttributes(array('global_alias' => $parsedIdentifier['alias']));
	  }
	  else
	    throw new TimerzInternalException('Unexpected situation in retrieveByIdentifier: timerIdentifier = ['.$timerIdentifier.'] parsedIdentifier = ' . print_r($parsedIdentifier, true));
    
	}
	
	/**
	 * @return Timer
	 */
	public static function retrieveForHomepage()
	{
  	return self::retrieve(self::getIdForHomepage());
	}
	
	/**
	 * Ротирует таймер для главной страницы и возвращает его id
	 * Таймер должен удовлетворять условиям:
	 * - иметь обложку
	 * - дата завершения больше чем через час
	 * - privacy <= PRIVACY_ALL
	 *
	 * @return integer
	 */
	public static function getIdForHomepage($rotate = false)
	{
	  $timerId = Yii::app()->cache->get('homepage_timer_id');
    if($timerId === false || $rotate)
    {
      $timerId = Timer::model()->dbConnection->createCommand('SELECT id FROM timer WHERE expires > UNIX_TIMESTAMP() + 3600 AND cover IS NOT NULL AND privacy = 0 ORDER BY RAND() LIMIT 1')->queryScalar();
      Yii::app()->cache->set('homepage_timer_id', $timerId, 3600);
    }
  	return $timerId;
	}
	
	public static function getPrivacyLabel($privacy, $contextMe = true)
	{
  	$options = self::getPrivacyOptions($contextMe);
  	if(!isset($options[$privacy]))
  	  throw new TimerzInternalException('Privacy not defined: ['.$privacy.']');
  	return $options[$privacy];
	}
	
	public static function getPrivacyOptions($contextMe = true)
	{
  	return array(
  	  Timer::PRIVACY_ALL     => Yii::t('timer', 'Access for all'),
  	  Timer::PRIVACY_PROFILE => $contextMe ? Yii::t('timer', 'Access from my page') : Yii::t('Access from user page'),
  	  Timer::PRIVACY_LINK    => $contextMe ? Yii::t('timer', 'Access whom I will give link') : Yii::t('timer', 'Access only by link'),
  	  Timer::PRIVACY_ONLYME  => Yii::t('timer', 'Access only for me'),
  	);
	}
	
	public static function getPrivacyIdentifier($privacy)
	{
  	switch($privacy)
  	{
    	case Timer::PRIVACY_ALL:      return 'all';
    	case Timer::PRIVACY_PROFILE:  return 'profile';
    	case Timer::PRIVACY_LINK:     return 'link';
    	case Timer::PRIVACY_ONLYME:   return 'onlyme';
    	default:
    	  return 'undefined';
  	}
	}
	
	public static function identifier($id, $alias = null, $userIdentifier = null)
  {
    if($alias)
    {
      if($userIdentifier)
        return $userIdentifier.'/'.$alias;
      else
        return $alias;
    }
    else
      return self::encodeHash($id);
  }
  
  public static function url($timerIdentifier, $action = 'show', $params = null, $absolute = false)
	{
	  if(is_array($action))
	  {
  	  $params = $action;
  	  $action = 'show';
	  }
	  elseif(is_bool($action))
	  {
  	  $absolute = $action;
  	  $action = 'show';
	  }
    if(is_null($params))
      $params = array();
    if(!is_array($params))
      $absolute = $params;
    $parsedIdentifier = self::parseIdentifier($timerIdentifier);
    $params['timerIdentifier'] = isset($parsedIdentifier['alias']) ? $parsedIdentifier['alias'] : $parsedIdentifier['hash'];
    if(isset($parsedIdentifier['userIdentifier']))
      $params['userIdentifier'] = $parsedIdentifier['userIdentifier'];
    return $absolute ? Yii::app()->createAbsoluteUrl('timer/'.$action, $params) : Yii::app()->createUrl('timer/'.$action, $params);
	}
	
	public static function parseIdentifier($timerIdentifier)
	{
	  $result = array();
  	$explode = explode('/', $timerIdentifier);
  	$result[is_null(self::decodeHash($explode[0])) ? 'alias' : 'hash'] = $explode[count($explode)-1];
  	if(count($explode) > 1)
  	  $result['userIdentifier'] = $explode[0];
  	 return $result;
	}
  
  /************************************************** PUBLIC **************************************************/
  
	public function tableName()
	{
		return 'timer';
	}
  
	public function rules()
	{
		$rules = array(
		  array('utcoffset' , 'numerical', 'min' => -840, 'max' => 840, 'allowEmpty' => false),
		  array('caption', 'required'),
			array('caption' , 'length', 'max' => 300, 'allowEmpty' => false),
			array('message' , 'length', 'max' => 1000, 'allowEmpty' => true),
			array('description' , 'length', 'max' => 1000, 'allowEmpty' => true),
			array('cover_url' , 'length', 'max' => 300, 'allowEmpty' => true),
			array('is_comments' , 'boolean'),
			array('privacy', 'in', 'range' => array_keys(self::getPrivacyOptions())),
			array('expires', 'expiresValidation')
		);
		if(Yii::app()->user->isGuest)
		{
  		$rules[] = array('email' , 'length', 'max' => 45, 'allowEmpty' => false);
  		$rules[] = array('email' , 'email');
		}
		return $rules;
	}
	
	public function getExpiresValue()
	{
  	if($this->expires_value)
      return $this->expires_value;
    elseif($this->getAttribute('expires'))
      return date('d.m.Y H:i:s', $this->getAttribute('expires'));
    else return null;
	}
	
	public function expiresValidation($attribute, $params)
  {
    $utcOffsetInSeconds = -$this->utcoffset*60;
    $this->expires_value = $this->getAttribute('expires');
    if(!$this->expires_value)
      return $this->addError('expires', Yii::t('timer', 'Timer must have expiration date'));
    $expires = Utility::strtotime($this->expires_value, $utcOffsetInSeconds);
    if(!$expires)
      return $this->addError('expires', Yii::t('timer', 'Unrecognized format'));
    if($expires < time())
      return $this->addError('expires', Yii::t('timer', 'Expiration date should be in the future'));
    $this->setAttribute('expires', $expires);
  }
	
	public function attributeLabels()
	{
		return array(
		  'caption' => Yii::t('timer', 'Caption'),
		  'message' => Yii::t('timer', 'Message'),
		  'cover_url' => 'URL картинки',
		  'cover' => 'Фоновая картинка',
		  'expires' => Yii::t('timer', 'Expiration'),
		  'is_comments' => Yii::t('timer', 'Comments'),
		  'description' => Yii::t('timer', 'Description'),
		  'email' => Yii::t('timer', 'Your email'),
		  'utcoffset' => 'UTC offset',
		  'privacy' => Yii::t('timer', 'Privacy'),
		);
	}
	
	public function relations()
	{
  	return array(
  	  'user' => array(self::BELONGS_TO, 'User', 'created_by'),
  	);
	}
	
	public function getLink($action = 'show', $params = null, $absolute = false)
	{
	  return self::url($this->getIdentifier(), $action, $params, $absolute);
	}
	
	public function getHash()
	{
  	return self::encodeHash($this->getPrimaryKey());
	}
	
	public function hasAlias()
	{
  	return $this->getAttribute('global_alias') || $this->getAttribute('local_alias');
	}
	
	public function isExpired()
	{
  	return $this->getAttribute('expires') <= time();
	}
	
	public function hasCover()
	{
  	return (bool) $this->getAttribute('cover');
	}
	
	public function setCover($cover)
	{
	  $this->_coverTmp = CoverTmp::retrieve($cover);
	}
	
	public function deleteCover()
	{
	  $this->_coverTmp = 'delete';
	}
	
	public function getCoverPath($thumbnail = false)
	{
  	return Yii::app()->params['coverPath'].DIRECTORY_SEPARATOR.User::encodeHash($this->getAttribute('created_by')).DIRECTORY_SEPARATOR.$this->getAttribute('cover').($thumbnail ? '_t' : '').'.jpg';
	}
	
	public function getCoverUrl($thumbnail = false)
	{
  	return Yii::app()->params['coverBaseUrl'].'/' . User::encodeHash($this->getAttribute('created_by')) . '/' . $this->getAttribute('cover') . ($thumbnail ? '_t' : '').'.jpg';
	}
	
	public function getMessage()
	{
  	$message = $this->getAttribute('message');
  	return $message ? $message : Yii::t('timer', 'Timer ended');
	}
	
	public function isComments()
	{
  	return $this->getAttribute('is_comments');
	}
	
	public function isMy()
	{
  	return !Yii::app()->user->isGuest && $this->getAttribute('created_by') == Yii::app()->user->getId();
	}
	
	public function getAuthor()
	{
  	return $this->getRelated('user');
	}
	
	public function incrementViewcount($force = false)
	{
	  if(!isset($_COOKIE["timer_view_".$this->getPrimaryKey()]) || $force)
	  {
  	  if(!$force)
  	  {
        $expire = time()+60*15; // просмотры увеличиваются максимум раз в 15 минут от одного пользователя
        setcookie("timer_view_".$this->getPrimaryKey(), time(), $expire);
      }
    	$sql = 'update ' . Timer::tableName() . ' set viewcount = viewcount + 1 where id = :timer_id';
    	$command = Timer::model()->dbConnection->createCommand($sql);
    	$command->bindParam(":timer_id", $this->getPrimaryKey(), PDO::PARAM_INT);
    	$command->execute();
    }
	}
  
  public function save($runValidation = true, $attributes = null)
  {
    $transaction = self::model()->dbConnection->beginTransaction();
    try
    {
      $result = parent::save($runValidation, $attributes);
      $transaction->commit();
      return $result;
    }
    catch(Exception $e)
    {
      $transaction->rollback();
      throw new TimerzInternalException($e);
      return false;
    }
  }
	
	public function last()
  {
    $this->getDbCriteria()->mergeWith(array(
      'order' => sprintf('`%s`.`created_at` DESC', $this->getTableAlias())
    ));
    return $this;
  }
  
  public function soon()
  {
    $this->getDbCriteria()->mergeWith(array(
      'order' => 'expires - UNIX_TIMESTAMP() ASC',
    ));
    return $this;
  }
  
  public function popular()
  {
    $this->getDbCriteria()->mergeWith(array(
      'order' => 'viewcount DESC',
    ));
    return $this;
  }
  
  public function ended()
  {
    $this->getDbCriteria()->mergeWith(array(
      'condition'=>'expires <= UNIX_TIMESTAMP()',
      'order' => 'UNIX_TIMESTAMP() - expires ASC',
    ));
    return $this;
  }
  
  public function notEnded()
  {
    $this->getDbCriteria()->mergeWith(array(
      'condition'=>'expires > UNIX_TIMESTAMP()',
    ));
    return $this;
  }
  
  public function privacyUnder($privacy)
  {
    $this->getDbCriteria()->mergeWith(array(
      'condition' => 'privacy <= :privacy',
      'params' => array(':privacy' => $privacy),
    ));
    return $this;
  }
  
  public function my()
  {
    if(!Yii::app()->user->isGuest)
      $this->ofUser(Yii::app()->user->getId());
    return $this;
  }
  
  public function userWatching($user)
  {
    if($user instanceof User)
      $userId = $user->getPrimaryKey();
    else
      $userId = (integer) $user;
    $this->getDbCriteria()->mergeWith(array(
      'join' => 'INNER JOIN timer_watch ON (timer_watch.user_id = :userId AND timer_watch.timer_id = t.id)',
      'params'=>array(':userId' => $userId),
    ));
    return $this;
  }
  
  public function ofUser($user)
  {
    if($user instanceof User)
      $userId = $user->getPrimaryKey();
    else
      $userId = (integer) $user;
    $this->getDbCriteria()->mergeWith(array(
      'condition'=>'created_by = :userId',
      'params'=>array(':userId' => $userId),
    ));
    return $this;
  }
  
  public function privacyIs($privacy)
  {
    return $privacy == $this->getAttribute('privacy');
  }
  
  public function privacyIsUnder($privacy)
  {
    return $this->getAttribute('privacy') <= $privacy;
  }
  
  public function privacyIsAbove($privacy)
  {
    return $this->getAttribute('privacy') >= $privacy;
  }

  /**
   * @param integer $user_id
   *
   * @return bool
   */
  public function watch($user_id)
  {
    if(!$user_id)
      throw new TimerzInternalException('user_id required');
    if($user_id == $this->getAttribute('created_by'))
      return false;
    $transaction = self::model()->dbConnection->beginTransaction();
    try
    {
      Timer::model()->dbConnection->createCommand('INSERT INTO timer_watch (user_id, timer_id) VALUES(:user_id, :timer_id)')
                    ->execute(array(':user_id' => $user_id, ':timer_id' => $this->getPrimaryKey()));
      $result = (bool) $this->changeWatchCount(1);
      $transaction->commit();
    }
    catch(Exception $e)
    {
      $transaction->rollback();
      $result = false;
    }
    return $result;
  }
  
  /**
   * @param integer $user_id
   *
   * @return bool
   */
  public function stopWatching($user_id)
  {
    if(!$user_id)
      throw new TimerzInternalException('user_id required');
    if($user_id == $this->getAttribute('created_by'))
      return false;
    $transaction = self::model()->dbConnection->beginTransaction();
    try
    {
      if(Timer::model()->dbConnection->createCommand('DELETE FROM timer_watch WHERE user_id = :user_id AND timer_id = :timer_id')
        ->execute(array(':user_id' => $user_id, ':timer_id' => $this->getPrimaryKey())))
      {
        $result = (bool) $this->changeWatchCount(-1);
      }
      else
        $result = false;
      $transaction->commit();
    }
    catch(Exception $e)
    {
      $transaction->rollback();
      throw new TimerzInternalException($e);
      $result = false;
    }
    return $result;
  }
  
  public function isWatchingByUser($user_id)
  {
    if(!$user_id)
      throw new TimerzInternalException('user_id required');
    if($user_id == $this->getAttribute('created_by'))
      throw new TimerzLogicException(Yii::t('timer', 'You can not watch your timer'));
    return Timer::model()->dbConnection->createCommand('SELECT EXISTS(SELECT 1 FROM timer_watch WHERE user_id = :user_id AND timer_id = :timer_id)')->queryScalar(array(':user_id' => $user_id, ':timer_id' => $this->getPrimaryKey()));
  }
  
  /**
   * Return global or local alias (wich is defined)
   *
   * return string
   */
  public function getAlias()
  {
    $globalAlias = $this->getAttribute('global_alias');
    $localAlias = $this->getAttribute('local_alias');
    return $globalAlias ? $globalAlias : $localAlias;
  }
  
  public function getIdentifier()
  {
    return self::identifier($this->getPrimaryKey(), $this->getAlias()/*, $userIdentifier */);
  }
  
  /**
   * Return array of Users watching this timer
   *
   * return array
   */
  public function getWatchers()
  {
    return User::model()->findAllBySql('SELECT * FROM user u INNER JOIN timer_watch tw ON (tw.user_id = u.id) WHERE tw.timer_id = :timer_id', array(':timer_id' => $this->getPrimaryKey()));
  }
  
  /************************************************** PROTECTED **************************************************/
  
  protected function changeWatchCount($delta)
  {
    return Timer::model()->dbConnection->createCommand('UPDATE timer SET watchcount = watchcount + :delta WHERE id = :timer_id')
             ->execute(array(':delta' => $delta, ':timer_id' => $this->getPrimaryKey()));
  }
  
  protected function beforeSave()
  {
    $result = parent::beforeSave();
    if($result)
    {      
      if($this->isNewRecord)
      {
        if(Yii::app()->user->isGuest)
        {
          $user = User::retrieveByEmail($this->email);
          if(!$user)
            $user = User::createByEmail($this->email);
          $this->setAttribute('created_by', $user->getPrimaryKey());
          $this->user = $user;
        }
        else
        {   
          if($this->user = User::instance())
          {
            $this->setAttribute('created_by', Yii::app()->user->getId());
          }
          else
          {
            $result = false;
            $userId = Yii::app()->user->getId();
            Yii::app()->user->logout();
            throw new TimerzInternalException('ID ['.$userId.'] of auth user has no row in table users');
          }
        }
      }
      if($this->_coverTmp)
      {
        if($this->_coverTmp == 'delete' && is_file($this->getCoverPath()))
          $this->_deleteCover();
        elseif($this->_coverTmp instanceof CoverTmp)
        {
          if($this->hasCover())
            $this->_deleteCover(); // сначала надо удалить старую картинку
          $this->setAttribute('cover', uniqid(''));
          $userCoverDir = Yii::app()->params['coverPath'].DIRECTORY_SEPARATOR.User::encodeHash($this->getAttribute('created_by'));
          if(!is_dir($userCoverDir))
            mkdir($userCoverDir);
          $this->_coverTmp->createThumbnail($this->getCoverPath(Timer::THUMBNAIL));
          if(rename($this->_coverTmp->getPath(), $this->getCoverPath()))
            $this->_coverTmp->delete();
          else
            $this->setAttribute('cover', null);
        }
      }
    }
    return $result;
  }
  
  protected function afterSave()
  {
    if($this->isNewRecord)
    {
      Notification::newTimer($this);
    }
  }
  
  /************************************************** PRIVATE STATIC **************************************************/
  
  private static function _hashids()
  {
    if(is_null(self::$_hashids))
      self::$_hashids = new Hashids\Hashids('sdfFE387sdfF83f027gdFsfbgjdcwnv38', 6);
    return self::$_hashids;
  }
  
  /************************************************** PRIVATE **************************************************/
  
  private function _deleteCover()
  {
    try
    {
      unlink($this->getCoverPath());
      unlink($this->getCoverPath(Timer::THUMBNAIL));
      $this->setAttribute('cover', null);
    }
    catch(Exception $e)
    {
      throw new TimerzWarningException($e->getMassage());
    }
  }
  
}