<?php

Yii::setPathOfAlias('Imagine',Yii::getPathOfAlias('application.vendors.Imagine'));

/**
 * @property string $hash
 * @property string $created_at
 */

class CoverTmp extends Model
{
  /************************************************** PUBLIC STATIC **************************************************/
  
  public static function createFromFile($inputFile)
  {
    $transaction = self::model()->dbConnection->beginTransaction();
    try
    {
      $coverTmp = new CoverTmp('insert');
      $coverTmp->setAttribute('hash', md5(time().'iuefbwe8iufibwebfjhsdgvfu32lf'));
      $coverTmp->save(false);
      $imagine = self::instanceImagine();
      $size = new Imagine\Image\Box(Yii::app()->params['coverWidth'], Yii::app()->params['coverHeight']);
      $mode = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
      $imagine->open($inputFile)
              ->thumbnail($size, $mode)
              ->save($coverTmp->getPath());
      $transaction->commit();
      return $coverTmp;
    }
    catch(Exception $e)
    {
      $transaction->rollback();
      throw $e;
    }
  }
  
  public static function retrieve($id)
	{
  	return CoverTmp::model()->findByPk($id);
	}
  
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
  
  public static function path($hash)
  {
    return Yii::app()->params['tmpCoverPath'].DIRECTORY_SEPARATOR.$hash.'.jpg';
  }
  
  public static function instanceImagine()
  {
    if(Yii::app()->params['Imagick'])
      return new Imagine\Imagick\Imagine();
    else
      return new Imagine\Gd\Imagine();
  }
  
  /************************************************** PUBLIC **************************************************/
  
  public function tableName()
	{
		return 'cover_tmp';
	}
	
	public function createThumbnail($outputFile)
	{
  	$imagine = self::instanceImagine();
    $size = new Imagine\Image\Box(Yii::app()->params['thumbnailCoverWidth'], Yii::app()->params['thumbnailCoverHeight']);
    $mode = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
    $imagine->open($this->getPath())
            ->thumbnail($size, $mode)
            ->save($outputFile);
	}
	
  public function getHash()
  {
    return $this->getAttribute('hash');
  }
	
	public function getUrl()
	{
  	return Yii::app()->params['tmpCoverBaseUrl'].'/'.$this->getAttribute('hash') . '.jpg';
	}
	
	public function getPath()
	{
	  return self::path($this->getAttribute('hash'));
	}
}