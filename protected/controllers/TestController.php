<?php

class TestController extends Controller
{
  public function filters()
  {
    return array(
      'debugChecker',
    );
  }
  
  public function actionEmail()
  {
    $message = new YiiMailMessage;
    $message->view = 'timerAdded';
    $message->setBody(array(), 'text/html');
    $message->addTo('2.vitalio@gmail.com');
    $message->subject = 'My TestSubject';
    $message->from = Yii::app()->params['adminEmail'];
    Yii::app()->mail->send($message);
  }
  
  public function actionSignin($userId)
  {
    //die('userId=['.$userId.']');
    Yii::app()->user->loginAsUser(User::retrieve($userId));
  }
  
  public function actionMail($view)
  {
    $this->layout = 'email';
    $this->renderPartial('//email/'.$view, array('user' => User::instance()));
  }
  
  public function filterDebugChecker($filterChain)
  {
    if(!YII_DEBUG)
      throw new CHttpException(404,'Страница не найдена');
    else
      $filterChain->run();
  }
}