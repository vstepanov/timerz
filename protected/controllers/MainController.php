<?php

class MainController extends Controller
{
  
  public function actionHomepage()
  {
    $timer = Timer::retrieveForHomepage();
    if($timer)
      $timer->incrementViewcount();
    $this->page = 'homepage';
    $this->pageTitle = Yii::t('app', 'Timerz - countdown timers');
    $this->hasPageTitlePostfix = false;
    $this->render('homepage', array('timer' => $timer));
  }
  
  /**
	 * This is the action to handle external exceptions.
	 */
  public function actionError()
  {
    if($error = Yii::app()->errorHandler->error)
    {
      $this->layout = 'narrowWithPageHeader';
      switch($error['code'])
      {
        case 403:
          $this->pageTitle = Yii::t('app', 'Access denied');
          $view = 'error403';
          break;
        case 404:
          $this->pageTitle = Yii::t('app', 'Page not found');
          $view = 'error404';
          break;
        default:
          $this->pageTitle = Yii::t('app', 'Error');
          $view = 'error500';
      }
      $this->render($view, array('error' => $error));
    }
  }
}