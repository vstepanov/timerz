<?php

class UserController extends Controller
{ 
  public function actionShow($userIdentifier, $type = null)
  {
    $user = User::retrieveByIdentifier($userIdentifier);
    if(!$user)
      throw new CHttpException(404);
    elseif($user->hasNickname() && !is_null(User::decodeHash($userIdentifier)))
      $this->redirect($user->getLink(), true, 301);
    $this->pageTitle = $user->getIdentifier();
    $this->page = 'userpage';
    $query = Timer::model();
    
    switch($type)
    {
      case null:
      case 'last':
        $query->last();
        break;
      case 'soon':
        $query->soon();
        break;
      case 'ended':
        $query->ended();
        break;
      case 'popular':
        $query->popular();
        break;
      case 'watching':
        $query->userWatching($user);
        break;
      /*case 'all':
        break;*/
      default:
        throw new CHttpException(404);
    }
    if($type != 'watching')
      $query->ofUser($user);
      
    if(!($type == 'ended'/* || $type == 'all'*/))
      $query->notEnded();
      
    if(!$user->isMe())
    {
      if($type == 'watching')
        $query->privacyUnder(Timer::PRIVACY_ALL);
      else
        $query->privacyUnder(Timer::PRIVACY_PROFILE);
    }
    
    $this->render('show', array('user' => $user, 'type' => $type, 'timersCriteria' => $query->getDbCriteria()));
  }
}