<?php

class TimerController extends Controller
{
  
  public function actionShow()
  {
    $timer = $this->_getTimer();
    if(!($timer->isMy() || $timer->privacyIsUnder(Timer::PRIVACY_LINK)))
      throw new CHttpException(403);
    $this->page = 'timerpage';
    $this->pageTitle = $timer->getAttribute('caption') ? $timer->getAttribute('caption') : $timer->getIdentifier();
    $timer->incrementViewcount();
    $params = array('maintimer', 'about', 'comments', 'description', 'centered', 'dynamic-cover', 'undertimer', 'watch', 'dynamic-bottom-border');
  	$this->render('show', array('timer' => $timer, 'params' => $params));
  }
  
  public function actionList($type)
  {
    $this->layout = 'narrowColWithPageHeader';
    
    $query = Timer::model();
    
    switch($type)
    {
      case 'last':
        $this->pageTitle = Yii::t('timer', 'Last added');
        $query->last();
        break;
      case 'soon':
        $this->pageTitle = Yii::t('timer', 'Coming soon');
        $query->soon();
        break;
      case 'ended':
        $this->pageTitle = Yii::t('timer', 'Recently ended');
        $query->ended();
        break;
      case 'popular':
        $this->pageTitle = Yii::t('timer', 'Popular timers');
        $query->popular();
        break;
      default:
        throw new CHttpException(404);
    }
    
    $query->privacyUnder(Timer::PRIVACY_ALL);
      
    if($type != 'ended')
      $query->notEnded();
    
    $this->render('list', array('timersCriteria' => $query->getDbCriteria()));
  }
  
  public function actionGetMessage($id)
  {
    $timer = Timer::retrieve($id);
    if(!$timer)
      throw new CHttpException(404);
    if($timer->isExpired())
      echo CHtml::encode($timer->getMessage());
    else
      echo "Timer not ended";
    Yii::app()->end();
  }
  
  public function actionStrtotime($time, $utcoffset = 0)
  {
    $utcOffsetInSeconds = -$utcoffset*60;
    echo Utility::strtotime($time, $utcOffsetInSeconds);
    Yii::app()->end();
  }
  
  public function actionWatching($action)
  {
    if(Yii::app()->user->isGuest)
    {
      $request = Yii::app()->getRequest();
      if(!$request->getIsAjaxRequest())
        Yii::app()->user->setReturnUrl($request->getUrl());
      $this->redirect(array('security/signup'));
    }
    $timer = $this->_getTimer();
    switch($action)
    {
      case 'watching':
        $timer->watch(Yii::app()->user->getId());
        break;
      case 'stopWatching':
        $timer->stopWatching(Yii::app()->user->getId());
        break;
      default:
        throw new CHttpException(404, 'action must be watching or stopWatching');
    }
    $this->redirect($timer->getLink());
  }
  
  public function actionEdit()
  {
    $timer = $this->_getTimer();
    if(Yii::app()->user->isGuest)
      Yii::app()->user->loginRequired();
    elseif(Yii::app()->user->id != $timer->getAttribute('created_by'))
      throw new CHttpException(403);
    $timer->setScenario('update');
    $this->pageTitle = Yii::t('timer', 'Edit timer');
    $this->_createOrEdit($timer);
  }
  
  public function actionCreate()
  {
    $this->pageTitle = Yii::t('timer', 'Create timer');
    $timer = new Timer('insert');
    $this->_createOrEdit($timer);
  }
  
  public function actionUploadCover()
  {
    $response = array('coverTmpHash' => '', 'error' => 0, 'errorMessage' => null);
    if(is_uploaded_file($_FILES['timer']['tmp_name']['cover']))
    {
      if(filesize($_FILES['timer']['tmp_name']['cover']) > Yii::app()->params['maxUploadCoverSize'])
      {
        $response['error'] = 3;
        $response['errorMessage'] = Yii::t('app', 'Max file size: {size}mb', array('{size}' => floor(Yii::app()->params['maxUploadCoverSize'] / 1048576)));
      }
      else
      {
        try
        {
          $coverTmp = CoverTmp::createFromFile($_FILES['timer']['tmp_name']['cover']);
          $response['coverTmpHash'] = $coverTmp->getHash();
          $response['coverUrl'] = $coverTmp->getUrl();
        }
        catch(Exception $e)
        {
          // TODO: log this error
          $response['error'] = 1;
          $response['errorMessage'] = Yii::t('app', 'Upload error, try another');
        }
      }
    }
    else
    {
      // TODO: log this error
      $response['error'] = 2;
      $response['errorMessage'] = Yii::t('app', 'Upload error, try another');
    }
    echo CJavaScript::jsonEncode($response);
    Yii::app()->end();
  }
  
  public function actionWatchers()
  {
    $this->layout = 'narrow';
    $timer = $this->_getTimer();
    $watchers = $timer->getWatchers();
    $this->render('watchers', array('timer' => $timer ,'users' => $watchers));
  }
  
  /************************************************** PRIVATE **************************************************/
  
  private function _createOrEdit($timer)
  {
    $this->layout = 'narrow';
    if(isset($_POST['timer']))
    {
      if(!isset($_POST['timer']['honeypot']) || $_POST['timer']['honeypot'])
      {
        echo "You are bot";
        throw new TimerzWarningException("You are bot");
        Yii::app()->end();
      }
      
      if(isset($_POST['timer']['honeypot']))
        unset($_POST['timer']['honeypot']);
      
      if(!isset($_POST['timer']['is_comments']))
        $_POST['timer']['is_comments'] = 0;
      
      if(isset($_POST['timer']['cover']))
      {
        if($_POST['timer']['cover'] == 'delete')
          $timer->deleteCover();
        else
          $timer->setCover($_POST['timer']['cover']);
        unset($_POST['timer']['cover']);
      }
      $timer->attributes = $_POST['timer'];
      $result = $timer->save();
      if(Yii::app()->request->isAjaxRequest)
      {
        $response = array(
          'result' => $result ? 'ok' : 'fail',
          'errors' => $timer->getErrors(),
          'url' => $result ? $timer->getLink() : null,
        );
        echo CJavaScript::jsonEncode($response);
        Yii::app()->end();
      }
      else
      {
        if($result)
          $this->redirect($timer->getLink());
      }
    }
    $this->render('create', array('timer' => $timer));
  }
  
  /**
   * @return Timer
   */
  private function _getTimer()
  {
    $params = $this->getActionParams();
    if(!isset($params['timerIdentifier']))
      throw new TimerzInternalException('timerIdentifier must be setted before calling TimerController::_getTimer');
    $timerIdentifier = $params['timerIdentifier'];
    if(isset($params['userIdentifier']))
      $timerIdentifier = $params['userIdentifier'] . '/' . $timerIdentifier;
    $timer = Timer::retrieveByIdentifier($timerIdentifier);
    if($timer->hasAlias() && !is_null(Timer::decodeHash($timerIdentifier)))
      $this->redirect($timer->getLink(), true, 301);
    return $timer;
  }
  
}