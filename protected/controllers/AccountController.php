<?php

class AccountController extends Controller
{
  public $layout = 'narrowWithPageHeader';
  
  private $user;
  
  public function actionIndex()
  {
    $this->layout = 'main';
    $this->pageTitle = Yii::t('app', 'Settings');
    $this->render('index', array('user' => $this->user));
  }
  
  public function actionChangePassword()
  {
    if(isset($_POST['user']))
    {
      $this->user->setScenario('changePassword');
      $this->user->attributes = $_POST['user'];
      if($this->user->validate(array('new_password', 'password_again', 'current_password')))
      {
        $this->user->setPassword($this->user->new_password);
        $this->user->save(false, array('password'));
        $this->redirect($this->createUrl('account/index'));
      }
    }
    $this->pageTitle = Yii::t('account', $this->user->hasPassword() ? 'Edit password' : 'Set password');
    $this->render('changePassword', array('user' => $this->user));
  }
  
  public function actionChangeEmail()
  {
    if(isset($_POST['user']))
    {
      $this->user->attributes = $_POST['user'];
      if($this->user->validate(array('email', 'current_password')))
      {
        $this->user->save(false, array('email'));
        $this->redirect($this->createUrl('account/index'));
      }
    }
    $this->pageTitle = Yii::t('account', $this->user->hasEmail() ? 'Edit email' : 'Set email');
    $this->render('changeEmail', array('user' => $this->user));
  }
  
  public function actionChangeNick()
  {
    if(isset($_POST['user']))
    {
      $this->user->attributes = $_POST['user'];
      if($this->user->validate(array('nickname', 'current_password')))
      {
        $this->user->save(false, array('nickname'));
        $this->redirect($this->createUrl('account/index'));
      }
    }
    $this->pageTitle = Yii::t('account', $this->user->hasNickname() ? 'Edit nickname' : 'Set nickname');
    $this->render('changeNick', array('user' => $this->user));
  }
  
  protected function beforeAction()
  {
    if(Yii::app()->user->isGuest)
      Yii::app()->user->loginRequired();
    $this->user = User::instance();
    if(!$this->user)
    {
      $userId = Yii::app()->user->getId();
      Yii::app()->user->logout();
      throw new TimerzInternalException('ID ['.$userId.'] of auth user has no row in table users');
    }
    return true;
  }
}