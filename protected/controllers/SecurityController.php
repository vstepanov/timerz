<?php

class SecurityController extends Controller
{
  
  public function filters()
  {
    return array(
      'redirectChecker - signout, auth, endpoint, restorePassword',
    );
  }
  
  public function filterRedirectChecker($filterChain)
  {
    if(!Yii::app()->user->isGuest)
      $this->redirect(Yii::app()->user->getLink());
    else
      $filterChain->run();
  }
  
  public function actionSignin()
  {
    $this->pageTitle = Yii::t('app', 'Sign in');
    $model = new SigninForm;
    if(isset($_POST['singin']))
		{
		  $model->attributes = $_POST['singin'];
			if($model->validate())
				$this->redirect(Yii::app()->user->returnUrl == '/' ? Yii::app()->user->getLink() : Yii::app()->user->returnUrl);
		}
  	$this->render('signin', array('model' => $model));
  }
  
  public function actionSignup()
  {
    $error = false;
    $email = null;
    if(isset($_POST['signup']) && isset($_POST['signup']['email']))
    {
      $email = trim($_POST['signup']['email']);
      $validator = new CEmailValidator;
      if($validator->validateValue($email))
      {
        if($user = User::createByEmail($email))
          $this->redirect(Yii::app()->user->returnUrl);
        else
          $error = Yii::t('security', 'User with this email is already registered');;
      }
      else
        $error = Yii::t('security', 'Invalid email format');
    }
    $this->pageTitle = Yii::t('app', 'Sign up');
  	$this->render('signup', array('error' => $error, 'email' => $email));
  }
  
  public function actionSignout()
	{
  	Yii::app()->user->logout();
  	$this->redirect(array('main/homepage'));
	}
	
	public function actionRestorePassword($user = null, $key = null)
	{
	  $this->layout = 'narrowWithPageHeader';
	  $this->pageTitle = Yii::t('security', 'Restore password');
	  if(!is_null($user) && !is_null($key))
	  {
  	  $user = User::retrieveByHash($user);
  	  if($user && $user->restorePasswordValid($key))
      {
        $user->resetPassword();
        if(Yii::app()->user->isGuest || Yii::app()->user->getId() != $user->getPrimaryKey())
          Yii::app()->user->loginAsUser($user);
        $this->redirect(array('account/changePassword'));
      }
      else
        $this->render('restorePasswordInvalid');
	  }
	  else
	  {
  	  $error = null;
  	  $login = null;
  	  if(isset($_POST['restorePassword']['login']))
  		{
  		  $login = trim($_POST['restorePassword']['login']);
  		  if(!$login)
  		    $error =  Yii::t('security', 'Enter your email or nickname');
  			elseif($user = User::retrieveByLogin($login))
  			{
  			  if($user->setRestorePasswordHash())
    			  $this->redirect(array('security/restorePasswordSended'));
          else
    			  $error = Yii::t('security', 'Unable to send link to password restore');
  			}
        else
          $error = Yii::t('security', 'User not found');
  		}
    	$this->render('restorePassword', array('error' => $error, 'login' => $login));
    }
	}
	
	public function actionRestorePasswordSended()
	{
	  $this->layout = 'narrowWithPageHeader';
	  $this->pageTitle = Yii::t('security', 'Restore password');
	  $this->render('restorePassword', array('sended' => 'yes'));
	}
	
	public function actionEndpoint()
	{
  	Hybrid_Endpoint::process();
	}
	
	public function actionAuth($provider)
	{
  	//
	}
}