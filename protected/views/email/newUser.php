<?php $this->beginContent('//layouts/email'); ?>
  <p>
    Теперь у Вас есть страница, на которой можно посмотреть список своих таймеров: <a href="<?php echo $user->getLink(true)?>"><?php echo $user->getLink(null, true)?></a>
  </p>
  <p>
    Для входа на сайт используйте учетные данные: <br>
    <ul>
      <li>Email: <strong><?php echo CHtml::encode($user->email) ?></strong></li>
      <li>Пароль: <span class="text-muted">не установлен</span></li>
    </ul>
    Чтобы установить пароль, перейдите по ссылке:<br><a href="<?php echo $user->getRestorePasswordLink(true)?>"><?php echo $user->getRestorePasswordLink(true)?></a>
  </p>
<?php $this->endContent(); ?>