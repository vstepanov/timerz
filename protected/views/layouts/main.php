<?php
$timersMenu = array(
  'popular' => Yii::t('timer', 'Popular'),
  'soon'    => Yii::t('timer', 'Soon'),
  'last'    => Yii::t('timer', 'Last'),
  'ended'   => Yii::t('timer', 'Ended'),
);
?>
<!DOCTYPE html>
<html>
<head>	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php echo CHtml::encode($this->pageTitle) . ($this->hasPageTitlePostfix ? ' - Timerz' : '') ?></title>
	
	<?php Yii::app()->clientScript->registerPackage('css') ?>
	<?php Yii::app()->clientScript->registerPackage('js') ?>
	
  <!-- CSS adjustments for browsers with JavaScript disabled -->
  <noscript><link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.fileupload-ui-noscript.css"></noscript>

	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/html5shiv.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/respond.min.js"></script>
    <![endif]-->

</head>
<body class="<?php if(!is_null($this->page)) echo $this->page?>">

<!-- Wrap all page content here -->
  <div id="wrap">
    <div id="dynamic-cover"></div>
    
    <div id="timerz-nav" class="compact">
      <div class="container">
        <a class="navbar-brand ticklogo" href="#">timer<span>:</span>z</a>
        <div class="account">
        <?php $this->beginClip('account') ?>
          <?php if(Yii::app()->user->isGuest):?>
            <a href="<?php echo $this->createUrl('security/signin')?>" <?php if($this->routeIs('security', 'signin')):?>class="active"<?php endif ?>><span class="glyphicon glyphicon-user"></span> <?php echo Yii::t('app', 'Sign in')?></a>
          <?php else: ?>
            <a href="<?php echo $this->createUrl('account/index')?>" <?php if($this->routeIs('account')):?>class="active"<?php endif ?>><span class="glyphicon glyphicon-cog"></span> <?php echo Yii::t('app', 'Settings')?></a>
          <?php endif ?>
        <?php $this->endClip() ?>
          <?php echo $this->clips['account'] ?>
        </div>
        <ul class="menu">
          <li class="compacted"><a href="/"<?php if($this->routeIs('main', 'homepage')):?> class="active"<?php endif ?>><span class="glyphicon glyphicon-home"></span> <?php echo Yii::t('timer', 'Home')?></a></li>
          <?php if(!Yii::app()->user->isGuest):?><li><a href="<?php echo User::url(User::identifier(Yii::app()->user->id, Yii::app()->user->name))?>"<?php if($this->routeIs('user', 'show', array('userIdentifier' => User::identifier(Yii::app()->user->id, Yii::app()->user->name)))):?> class="active"<?php endif ?>><span class="glyphicon glyphicon-time"></span> <?php echo Yii::t('timer', 'My timers')?></a></li><?php endif ?>
          <li><a href="<?php echo $this->createUrl('timer/create') ?>"<?php if($this->routeIs('timer', 'create')):?> class="active"<?php endif ?>><span class="glyphicon glyphicon-plus"></span> <?php echo Yii::t('timer', 'New timer')?></a></li>
          <?php foreach($timersMenu as $type => $name):?>
            <li><a href="<?php echo $this->createUrl('timer/list', array('type' => $type)) ?>"<?php if($this->routeIs('timer', 'list', array('type' => $type))):?> class="active"<?php endif ?>><?php echo $name?></a></li>
          <?php endforeach ?>
          <li class="account"><?php echo $this->clips['account'] ?></li>
        </ul>
        
      </div>
    </div>

    <!-- Begin page content -->
    
    <div id="content">
      <div class="container">
        <?php echo $content ?>
      </div>
    </div>
    
  </div>
  
  <div id="footer">
    <div class="container">
      <p class="text-muted credit"><span class="item">&copy; Timerz 2013</span> <?php if(YII_DEBUG || (isset($_GET['debug']) && $_GET['debug'] = '1')):?><span class="item"><?php echo sprintf('%.2fms %.2fmb', Yii::getLogger()->getExecutionTime()*1000, memory_get_peak_usage()/1048576)?></span><?php endif ?></p>
    </div>
  </div>
	
	<?php if(!YII_DEBUG):?>
	<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-44053205-1', 'timerz.org');
    ga('send', 'pageview');
  </script>
  <?php endif ?>
  
</body>
</html>