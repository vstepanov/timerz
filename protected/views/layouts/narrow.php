<?php $this->beginContent('//layouts/main'); ?>
<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
  <?php echo $content ?>
</div>
<?php $this->endContent(); ?>