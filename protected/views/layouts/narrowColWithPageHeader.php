<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
  <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    <div class="page-header">
      <h1><?php echo CHtml::encode($this->pageTitle) ?></h1>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col">
    <?php echo $content ?>
  </div>
</div>
<?php $this->endContent(); ?>