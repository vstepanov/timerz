<html>
<head>	
	<meta charset="utf-8">
	<title>Timerz</title>
	<style type="text/css">
	  body {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:16px;line-height: 1.428571429em;padding:5px;}
	  html,input,textarea {-webkit-font-smoothing: antialiased !important;}
	  .logo {font-weight: bold;font-size: 1.7em;background-color: #428bca;color:#fff;text-decoration: none;padding:15px;display: block;float:left;margin-bottom: 15px;}
	   .text-muted {color: #666;}
	  .content {clear:both;}
	  h1, h2 {font-weight: normal;}
	</style>
</head>
<body>
  <div>
    <a class="logo" href="http://timerz.org">timer:z</a>
    <p class="content">
      <?php echo $content ?>
    </p>
  </div>
</body>
</html>