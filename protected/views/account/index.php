<div class="settings">
  
  <div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-3">
      <div class="page-header">
        <h1><?php echo CHtml::encode($this->pageTitle) ?></h1>
      </div>
    </div>
  </div>
  
  <div class="row setting">
    <div class="col-lg-4 col-md-4 col-sm-4 name">
      <?php echo Yii::t('user', 'Nickname')?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 value">
      <?php if(!$user->hasNickname()):?>
        <span class="text-muted"><?php echo Yii::t('account', 'not set')?></span>
      <?php else:?>
        <?php echo $user->getNickname() ?>
      <?php endif ?>
      <a href="<?php echo $this->createUrl('account/changeNick')?>"><?php echo Yii::t('account', $user->hasNickname() ? 'Edit' : 'Set') ?></a>
    </div>
  </div>
  
  <div class="row setting">
    <div class="col-lg-4 col-md-4 col-sm-4 name">
      Email
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 value">
      <?php if(!$user->email):?>
        <span class="text-muted"><?php echo Yii::t('account', 'not set')?></span>
      <?php else:?>
        <?php echo $user->email ?>
      <?php endif ?>
      <a href="<?php echo $this->createUrl('account/changeEmail')?>"><?php echo Yii::t('account', $user->hasEmail() ? 'Edit' : 'Set') ?></a>
    </div>
  </div>
  
  <div class="row setting">
    <div class="col-lg-4 col-md-4 col-sm-4 name">
      <?php echo Yii::t('user', 'Password')?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 value">
      <?php if(!$user->hasPassword()):?>
        <span class="text-muted"><?php echo Yii::t('account', 'not set')?></span>
      <?php else:?>
        <?php echo Yii::t('account', 'defined')?>
      <?php endif ?>
      <a href="<?php echo $this->createUrl('account/changePassword')?>"><?php echo Yii::t('account', $user->hasPassword() ? 'Edit' : 'Set') ?></a>
    </div>
  </div>
  
  <div class="row setting">
    <div class="col-lg-4 col-md-4 col-sm-4 name">
      
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 value">
      <a href="<?php echo $this->createUrl('security/signout')?>"><?php echo Yii::t('security', 'Sign out')?></a>
    </div>
  </div>
  
</div>