<form action="<?php echo $this->createUrl('account/changeEmail') ?>" method="post">
  <div class="form-group <?php if($user->hasErrors('email')) echo 'has-error'?>">
    <label class="control-label"><?php echo $user->getAttributeLabel('email')?></label>
    <input type="text" name="user[email]" class="form-control" value="<?php echo CHtml::encode($user->getAttribute('email'))?>">
    <p class="error help-block"><?php echo $user->getError('email')?></p>
  </div>
  <?php $this->renderPartial('_accountCommonFormFields', array('user' => $user))?>
</form>