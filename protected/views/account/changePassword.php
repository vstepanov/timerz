<form action="<?php echo $this->createUrl('account/changePassword') ?>" method="post">
  <div class="form-group <?php if($user->hasErrors('new_password')) echo 'has-error'?>">
    <label class="control-label"><?php echo $user->getAttributeLabel('new_password')?></label>
    <input type="password" name="user[new_password]" class="form-control" value="<?php echo CHtml::encode($user->getAttribute('new_password'))?>">
    <p class="error help-block"><?php echo $user->getError('new_password')?></p>
  </div>
  <div class="form-group <?php if($user->hasErrors('password_again')) echo 'has-error'?>">
    <label class="control-label"><?php echo $user->getAttributeLabel('password_again')?></label>
    <input type="password" name="user[password_again]" class="form-control">
    <p class="error help-block"><?php echo $user->getError('password_again')?></p>
  </div>
  <?php if($user->hasPassword()):?>
    <div class="form-group <?php if($user->hasErrors('current_password')) echo 'has-error'?>">
      <label class="control-label"><?php echo $user->getAttributeLabel('current_password')?> <span class="text-muted"><?php echo Yii::t('account', 'to confirm')?></span></label>
      <input type="password" name="user[current_password]" class="form-control">
      <p class="error help-block"><?php echo $user->getError('current_password')?></p>
    </div>
  <?php endif ?>
  <?php $this->renderPartial('_accountCommonFormFields', array('user' => $user))?>
</form>