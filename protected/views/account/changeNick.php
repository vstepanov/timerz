<form action="<?php echo $this->createUrl('account/changeNick') ?>" method="post">
  <div class="form-group <?php if($user->hasErrors('nickname')) echo 'has-error'?>">
    <label class="control-label"><?php echo $user->getAttributeLabel('nickname')?> <span class="text-muted"><?php echo Yii::t('account', 'at least 4 characters, letters and numbers')?></span></label>
    <input type="text" name="user[nickname]" class="form-control" value="<?php echo CHtml::encode($user->getAttribute('nickname'))?>">
    <p class="error help-block"><?php echo $user->getError('nickname')?></p>
  </div>
  <?php $this->renderPartial('_accountCommonFormFields', array('user' => $user))?>
</form>