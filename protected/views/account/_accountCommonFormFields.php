<div class="form-group text-center">
  <button type="submit" class="btn btn-success btn-lg"><?php echo Yii::t('app', 'Save')?></button>
  <a class="btn btn-lg btn-warning" href="<?php echo $this->createUrl('account/index')?>"><?php echo Yii::t('app', 'Cancel')?></a>
</div>