<p class="lead">
  <?php echo Yii::t('app', 'This is rarely the case. We have to understand the reasons. Sorry about that!')?>
</p>

<p>
  <a href="<?php echo Yii::app()->user->returnUrl ?>" class="btn btn-lg btn-info"><?php echo Yii::t('app', 'Go back')?></a>
</p>