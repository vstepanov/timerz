<?php if($timer) echo $this->renderPartial('//timer/show', array('timer' => $timer, 'params' => array('timerlink', 'maintimer', 'dynamic-cover')), true)?>
<div class="row">
  <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col">
    <div class="jumbotron promo">
      <h2><?php echo Yii::t('timer', 'Create your own timer')?> <span class="text-muted"><?php echo Yii::t('timer', 'and share it with friends')?></span></h2>
      <?php echo $this->renderPartial('//timer/_form', array('timer' => new Timer('insert'), 'showCancel' => false), true)?>
    </div>
  </div>
</div>

<div class="dynamic-bottom-border" style="clear:both;"></div>
  
<div class="container dynamic-under featurette">

  <hr class="featurette-divider">
  
  <h2 class="featurette-heading text-center"><?php echo Yii::t('timer', 'Waiting can be fun')?></h2>

  <p class="lead" style="text-align:justify;">
    Представьте, что Вы на оживленной площади, в центре которой огромные часы отсчитывают время до важного события, а все вокруг, трепетно ожидая его наступления, общаются с единомышленниками и делятся слухами...
  </p>
  
  <h2 class="featurette-heading text-center"><span class="text-muted">примерно это и есть</span> <strong>timerz </strong><span class="text-muted">:-)</span></h2>
  
  <hr class="featurette-divider">
  
  <?php /*
  
  <h2 class="featurette-heading text-center">Но:</h2>
  
  <ul>
    <li>часы отсчитывают время до Вашего события</li>
    <li>можно собрать вместе единомышленников невзирая на расстояния</li>
    <li>когда время выйдет, все увидят Ваше послание</li>
  </ul>
  
  <hr class="featurette-divider">*/?>
  
  <?php /*<div class="row featurette">
    <div class="col-md-6">
      <h2 class="featurette-heading">Зачем? <span class="text-muted">Во-первых это прикольно :)</span></h2>
      <p class="lead">
        Вы сможете:
        смотреть список своих таймеров, редактировать таймеры, выбирать имя ссылки, отключать комментарии
      </p>
    </div>
    <div class="col-md-6">
      <img src="/img/demo1.png" class="img-responsive">
    </div>
  </div>
  
  <hr class="featurette-divider">
  
  <div class="row featurette">
    <div class="col-md-6">
      <img src="/img/demo2.png" class="img-responsive">
    </div>
    <div class="col-md-6">
      <h2 class="featurette-heading">Что дальше? <span class="text-muted">Во-первых это прикольно :)</span></h2>
      <p class="lead">
        Вы сможете:
        смотреть список своих таймеров, редактировать таймеры, выбирать имя ссылки, отключать комментарии
      </p>
    </div>
  </div>*/?>
</div>