<p class="lead">
  <?php echo Yii::t('app', 'The page does not exist or has been deleted.')?>
</p>

<p>
  <a href="<?php echo Yii::app()->user->returnUrl ?>" class="btn btn-lg btn-info"><?php echo Yii::t('app', 'Go back')?></a>
</p>
