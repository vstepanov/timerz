<p class="lead">
  <?php echo Yii::t('app', 'The page is protected by privacy settings.')?>
</p>

<p>
  <a href="<?php echo Yii::app()->user->returnUrl ?>" class="btn btn-lg btn-info"><?php echo Yii::t('app', 'Go back')?></a>
</p>
