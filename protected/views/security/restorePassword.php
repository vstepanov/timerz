<?php if(isset($sended)):?>

<p class="lead text-success"><?php echo Yii::t('security', 'Email with instructions to reset your password sent to your email')?></p>

<?php else:?>

<form action="<?php echo $this->createUrl('security/restorePassword') ?>" method="post">
  <div class="form-group <?php if($error) echo 'has-error'?>">
    <input type="text" name="restorePassword[login]" placeholder="<?php echo Yii::t('security', 'Enter your email or nickname')?>" class="form-control" value="<?php if($login) echo CHtml::encode($login)?>">
    <p class="error help-block"><?php if($error) echo $error ?></p>
  </div>
  <div class="form-group text-center">
    <button type="submit" class="btn btn-success btn-lg"><?php echo Yii::t('app', 'Send')?></button>
  </div>
</form>

<?php endif ?>