<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
  <div class="page-header">
    <h1><?php echo CHtml::encode($this->pageTitle) ?></h1>
  </div>
  <form action="<?php echo $this->createUrl('security/signin') ?>" method="post">
    <div class="form-group <?php if($model->hasErrors('login')) echo 'has-error'?>">
      <input type="text" name="singin[login]" placeholder="<?php echo $model->getAttributeLabel('login')?>" class="form-control" value="<?php echo $model->login?>">
      <p class="error help-block"><?php echo $model->getError('login')?></p>
    </div>
    <div class="form-group <?php if($model->hasErrors('password')) echo 'has-error'?>">
      <input type="password" name="singin[password]" class="form-control" placeholder="<?php echo $model->getAttributeLabel('password')?>">
      <p class="error help-block"><?php echo $model->getError('password')?></p>
    </div>
    <div class="form-group text-center">
      <button type="submit" class="btn btn-success btn-lg"><?php echo Yii::t('security', 'Sign in')?></button>
    </div>
  </form>
  
  <p class="text-center"><a href="<?php echo $this->createUrl('security/signup')?>"><?php echo Yii::t('security', 'New to Timerz?')?></a></p>
  <p class="text-center"><a href="<?php echo $this->createUrl('security/restorePassword')?>"><?php echo Yii::t('security', 'Forgot password?')?></a></p>
</div>