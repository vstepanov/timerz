<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
  <div class="page-header text-center">
    <h1><?php echo CHtml::encode($this->pageTitle) ?></h1>
  </div>
  <form action="<?php echo $this->createUrl('security/signup') ?>" method="post">
    <div class="form-group <?php if($error) echo 'has-error'?>">
      <input type="text" name="signup[email]" placeholder="<?php echo Yii::t('security', 'Enter your email')?>" class="form-control text-center" value="<?php if(!is_null($email)) echo $email?>">
      <p class="error help-block text-center"><?php if($error) echo $error?></p>
    </div>
    <div class="form-group text-center">
      <button type="submit" class="btn btn-success btn-lg"><?php echo Yii::t('security', 'Continue')?></button>
    </div>
  </form>
  <p class="text-center"><a href="<?php echo $this->createUrl('security/signin')?>"><?php echo Yii::t('security', 'Already have account?')?></a></p>
</div>