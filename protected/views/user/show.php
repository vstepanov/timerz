<?php
$timersMenu = array(
  'soon'      => Yii::t('timer', 'Soon'),
  'last'      => Yii::t('timer', 'Last'),
  'popular'   => Yii::t('timer', 'Popular'),
  'ended'     => Yii::t('timer', 'Ended'),
  'watching'  => Yii::t('timer', 'Watching'),
  /*'all'       => 'Все',*/
);
?>
<div class="user">
  <div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-3">
      <div class="page-header">
        <h1>
          <?php echo $user->getIdentifier() ?>  
        </h1>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-3 col">
      <ul class="menu">
        <?php foreach($timersMenu as $t => $name):?>
          <li>
            <a href="<?php echo $user->getLink(array('type' => $t)) ?>"<?php if($type == $t):?> class="active"<?php endif ?>>
              <?php if($t == 'watching'):?><span class="glyphicon glyphicon-eye-open"></span><?php endif ?>
              <?php echo $name?>
            </a>
          </li>
        <?php endforeach ?>
      </ul>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-8 col">
      <?php $this->widget('application.widgets.TimersListWidget', array('criteria' => $timersCriteria, 'author' => $user, 'params' => array('about','timerlink')))?>
    </div>
  </div>
</div>