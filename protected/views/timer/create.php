<div class="page-header">
  <h1><?php echo CHtml::encode($this->pageTitle) ?></h1>
  
  <p class="lead">
    <?php if($timer->isNewRecord) echo Yii::t('timer', 'Create countdown timer to your event and share it with family, friends or whole world!')?>
  </p>
</div>

<?php echo $this->renderPartial('_form', array('timer' => $timer, 'showTimer' => true, 'showCancel' => true), true)?>