<?php
  /*
    params:
      about
      about-at-bottom
      timerlink
      maintimer
      undertimer
      comments
      description
      centered
      dynamic-cover
      dynamic-bottom-border
      thumbnail
      watch
  */
  $params = isset($params) ? $params : array();
  $isTimerLink = in_array('timerlink', $params);
  $isMaintimer = in_array('maintimer', $params);
  $isWatch = in_array('watch', $params) && !$timer->isMy();
  if(in_array('about', $params) || $isWatch)
    $timerAuthor = isset($author) ? $author : $timer->getAuthor();
  if($isWatch)
    $userIsWatching = Yii::app()->user->isGuest ? false : $timer->isWatchingByUser(Yii::app()->user->getId());
  $aboutAtBottom = in_array('about-at-bottom', $params);
  $isComments = in_array('comments', $params) && $timer->isComments();
  $isDescription = $timer->getAttribute('description') && in_array('description', $params);
  $isThumbnail = in_array('thumbnail', $params) || !is_null(SCREEN_WIDTH) && SCREEN_WIDTH <= 480;
?>
<div class="timer-container <?php if($isTimerLink) echo 'has_timerlink'?>">

  <div
    class="timer<?php if($isMaintimer) echo ' maintimer'?><?php if($timer->isExpired()) echo ' expired'?>"
    id="timer<?php echo $timer->getPrimaryKey()?>"
    data-expires="<?php echo $timer->getAttribute('expires')?>"
    data-cover-url="<?php if($timer->hasCover()) echo Utility::encode_basename($timer->getCoverUrl($isThumbnail))?>"
    data-centered="<?php echo $isMaintimer && in_array('centered', $params) ?>"
    data-dynamic-cover="<?php echo $isMaintimer && in_array('dynamic-cover', $params) ?>"
  ><div class="timer-inner">
  
    <?php if($isTimerLink):?><a href="<?php echo $timer->getLink()?>" class="timerlink"><?php endif ?>
    
      <div class="cmt-container">
    
        <h1 class="caption<?php if($timer->isExpired()) echo ' hide'?>"><?php echo CHtml::encode($timer->getAttribute('caption')) ?></h1>
        
        <div class="message<?php if(!$timer->isExpired()) echo ' hide'?>"><?php if($timer->isExpired()) echo CHtml::encode($timer->getMessage()) ?></div>
        
        <div class="tick<?php if($timer->isExpired()) echo ' hide'?>">
          <span class="d<?php if($timer->isExpired()) echo ' hide'?>"><span class="count"></span> <span class="name"></span></span>
          <span class="time">
            <span class="h<?php if($timer->isExpired()) echo ' hide'?>"></span>
            <span class="m<?php if($timer->isExpired()) echo ' hide'?>"></span>
            <span class="s"><?php if($timer->isExpired()) echo '00'?></span>
          </span>
        </div>
        
      </div>
      
    <?php if($isTimerLink):?></a><?php endif ?>
    
    <?php $this->beginClip('about') ?>
    <div class="about-container">
      
      <?php if(in_array('about', $params)):?>
      <ul>
        <li title="<?php echo Yii::t('timer', 'Created by')?>">
          <span class="glyphicon glyphicon-user"></span> <a href="<?php echo $timerAuthor->getLink() ?>" <?php if($timer->isMy()):?>class="me"<?php endif?>><?php echo $timerAuthor->getIdentifier() ?></a></li>
        <li>
          <?php echo Yii::t('timer', '{n} view|{n} views', $timer->getAttribute('viewcount'))?></li>
        <li>
          <?php if($timer->isMy()):?><a href="<?php echo $timer->getLink('watchers')?>"><?php endif ?>
            <?php echo Yii::t('timer', '{n} watcher|{n} watchers', $timer->getAttribute('watchcount'))?>
          <?php if($timer->isMy()):?></a><?php endif ?>
          </li>
        <li title="<?php echo Yii::t('timer', 'End at')?>">
          <span class="glyphicon glyphicon-time"></span>
          <span class="timestamp-replace" data-timestamp="<?php echo $timer->getAttribute('expires') ?>"></span>
          <?php if($timer->isMy() && $timer->privacyIsAbove(Timer::PRIVACY_PROFILE)):?>
            <span class="glyphicon glyphicon-lock privacy-<?php echo Timer::getPrivacyIdentifier($timer->getAttribute('privacy'))?>" title="<?php echo Timer::getPrivacyLabel($timer->getAttribute('privacy'), $timer->isMy())?>"></span>
          <?php endif ?></li>
        <?php if($timer->isMy()):?>
        <li>
          <span class="glyphicon glyphicon-pencil"></span> <a href="<?php echo $timer->getLink('edit')?>"><?php echo Yii::t('timer', 'Edit')?></a></li>
        <?php endif ?>
      </ul>
      <?php endif ?>
      <?php if($isWatch):?>
        <div class="watch">
          <form action="<?php echo $timer->getLink('watching', array('action' => ($userIsWatching ? 'stopWatching' : 'watching')))?>" method="post">
            <button type="submit" class="btn btn-<?php echo $userIsWatching ? 'warning' : 'info'?>">
              <span class="glyphicon glyphicon-eye-<?php echo $userIsWatching ? 'close' : 'open'?>"></span> <?php echo Yii::t('timer', $userIsWatching ? 'Stop watching' : 'Watch') ?>
            </button>
          </form>
        </div>
      <?php endif ?>
    </div>
    <?php $this->endClip() ?>
    
    <?php if(!$aboutAtBottom):?>
      <div class="about-wrapper"><?php echo $this->clips['about'] ?></div>
    <?php endif ?>
    
    </div><!-- /.timer-inner -->
    
    <?php if(in_array('dynamic-bottom-border', $params)):?><div class="dynamic-bottom-border" style="clear:both;"></div><?php endif ?>
    
    <?php if(in_array('undertimer', $params) && ($isDescription || $isComments)):?>
    
    <hr class="only-uncovered">
    
    <div class="dynamic-under col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
    
      <div class="description <?php if(!$isDescription) echo 'hide'?>"><?php if($timer->getAttribute('description')) echo CHtml::encode($timer->getAttribute('description')) ?></div>
      
      <?php if($isComments):?>
        
        <div class="comments">
          <div id="disqus_thread"></div>
          <script type="text/javascript">
              /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
              var disqus_shortname = 'timerz'; // required: replace example with your forum shortname
              <?php if(YII_DEBUG):?>var disqus_developer = 1;<?php endif ?>
      
              /* * * DON'T EDIT BELOW THIS LINE * * */
              (function() {
                  var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                  dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                  (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
              })();
          </script>
          <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
          <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
        </div>
      <?php endif ?>
    
    </div><!-- /.undertimer -->
    <?php endif ?>
  
  </div>
  
  <?php if($aboutAtBottom) echo $this->clips['about'] ?>
  
</div>