<form class="timer-form <?php if($timer->isNewRecord) echo 'is_new'?> <?php if($timer->hasCover()) echo 'has_cover'?>" action="<?php echo $timer->isNewRecord ? $this->createUrl('timer/create') : $timer->getLink('edit') ?>" method="post" enctype="multipart/form-data">

  <input type="hidden" id="timer_utcoffset" class="value" name="timer[utcoffset]">
  <input type="hidden" class="value" name="timer[honeypot]">
  <input type="hidden" id="timer_cover" class="value" name="timer[cover]">
  <script>document.getElementById("timer_utcoffset").value = (new Date()).getTimezoneOffset();</script>
  
  <div class="form-group required <?php if($timer->hasErrors('caption')) echo 'has-error'; elseif($timer->getAttribute('caption')) echo 'has-success';?>">
    <label class="control-label" for="timer_caption"><?php echo $timer->getAttributeLabel('caption')?></label>
    <input type="text"  name="timer[caption]" id="timer_caption" class="form-control value" placeholder="<?php echo Yii::t('timer', 'ex: before my birthday left')?>" value="<?php echo CHtml::encode($timer->getAttribute('caption'))?>">
    <p class="error help-block"><?php echo $timer->getError('caption')?></p>
  </div>
  
  <div class="form-group required <?php if($timer->hasErrors('expires')) echo 'has-error'; elseif($timer->getExpiresValue()) echo 'has-success';?>">
    <label class="control-label" for="timer_expires"><?php echo $timer->getAttributeLabel('expires')?></label>
    <input type="text" name="timer[expires]" id="timer_expires" class="form-control value" placeholder="<?php echo Yii::t('timer', 'ex: {datetime} or {time}', array('{datetime}' => '21.10.2015 16:29', '{time}' => '23:59'))?>" value="<?php echo CHtml::encode($timer->getExpiresValue())?>" data-timestamp="<?php echo $timer->getAttribute('expires')?>">
    <p class="error help-block"><?php echo $timer->getError('expires')?></p>
  </div>
  
  <?php $this->beginClip('message') ?>
    <div class="form-group <?php if($timer->hasErrors('message')) echo ' has-error'; elseif($timer->getAttribute('message')) echo 'has-success'?>">
      <label class="control-label" for="timer_message"><?php echo $timer->getAttributeLabel('message')?></label>
      <textarea  name="timer[message]" id="timer_message" class="form-control value" rows="3" placeholder="<?php echo Yii::t('timer', 'will show after timer expiration')?>"><?php echo $timer->getAttribute('message')?></textarea>
      <p class="error help-block"><?php echo $timer->getError('message')?></p>
    </div>
  <?php $this->endClip() ?>
  
  <?php if(Yii::app()->user->isGuest):?>
    <div class="form-group required <?php if($timer->hasErrors('email')) echo 'has-error'; elseif($timer->email) echo 'has-success';?>">
      <label class="control-label" for="timer_email"><?php echo $timer->getAttributeLabel('email')?></label>
      <input type="text" name="timer[email]" id="timer_email" class="form-control value" value="<?php echo CHtml::encode($timer->email)?>" placeholder="<?php echo Yii::t('timer', 'where to send the link to control')?>">
      <p class="error help-block"><?php echo $timer->getError('email')?></p>
    </div>
  <?php else: ?>
    <?php echo $this->clips['message'] ?>
  <?php endif ?>
  
  <div class="form-group additional-switch">
    <a href="#" class="additional-show"><span class="glyphicon glyphicon-chevron-down"></span> <?php echo Yii::t('timer', 'Additional attributes')?></a>
    <a href="#" class="additional-hide"><span class="glyphicon glyphicon-chevron-up"></span> <?php echo Yii::t('timer', 'Additional attributes')?></a>
  </div>
  
  
  <div class="additional">
    
    <?php if(Yii::app()->user->isGuest) echo $this->clips['message'] ?>
    
    <?php if(isset($showTimer) && $showTimer):?>
    <div class="timers example">
      <?php echo $this->renderPartial('//timer/show', array('timer' => $timer, 'params' => array('maintimer')), true)?>
    </div>
    <?php endif ?>
    
    <div class="form-group cover">
      
      
        <span class="btn btn-primary fileinput-button">
          <span class="choose"><?php echo Yii::t('timer', 'Upload picture')?></span>
          <span class="change"><?php echo Yii::t('timer', 'Edit picture')?></span>
          <input type="file" name="timer[cover]" id="timer_cover_upload" data-url="<?php echo $this->createUrl('timer/uploadCover')?>">
        </span>
        <button type="button" class="btn btn-danger delete">
          <i class="glyphicon glyphicon-trash"></i>
          <span><?php echo Yii::t('app', 'Delete')?></span>
        </button>
      
      <div class="progress">
        <div class="progress-bar" role="progressbar"></div>
      </div>
    </div>

    <div class="form-group <?php if($timer->hasErrors('description')) echo ' has-error'; elseif($timer->getAttribute('description')) echo 'has-success'?>">
      <label class="control-label" for="timer_description"><?php echo $timer->getAttributeLabel('description')?></label>
      <textarea  name="timer[description]" id="timer_description" class="form-control value" rows="3" placeholder="<?php echo Yii::t('timer', 'displayed under timer')?>"><?php echo $timer->getAttribute('description')?></textarea>
      <p class="error help-block"><?php echo $timer->getError('description')?></p>
    </div>
    
    <div class="form-group">

      <label class="control-label" for="timer_privacy"><?php echo $timer->getAttributeLabel('privacy')?></label>
      <select class="value" id="timer_privacy" name="timer[privacy]">
        <?php foreach(Timer::getPrivacyOptions() AS $privacyValue => $privacyLabel):?>
          <option<?php if($timer->privacyIs($privacyValue)):?> selected<?php endif ?> value="<?php echo $privacyValue ?>"><?php echo $privacyLabel?></option>
        <?php endforeach ?>
      </select>
    </div>
    
    <div class="checkbox">
      <label>
        <input type="checkbox" name="timer[is_comments]" class="value" value="1" <?php if($timer->isComments()):?>checked="checked"<?php endif ?>> <?php echo $timer->getAttributeLabel('is_comments')?> <span class="text-muted"><?php echo Yii::t('timer', 'on timer page')?></span>
      </label>
    </div>
    
  </div>
  
  <div class="form-group text-center buttons">
    <button type="submit" class="btn btn-success btn-lg" data-loading-text="Загрузка..."><?php echo $timer->isNewRecord ? Yii::t('timer', 'Create timer') : Yii::t('app', 'Save') ?></button>
    <?php if(isset($showCancel) && $showCancel):?>
      <a class="btn btn-lg btn-warning" href="<?php echo $this->createUrl($timer->isNewRecord ? 'main/homepage' : $timer->getLink())?>"><?php echo Yii::t('app', 'Cancel')?></a>
    <?php endif ?>
  </div>
  
</form>