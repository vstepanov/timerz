<div class="page-header">
  <h1><?php echo Yii::t('timer', 'Watchers of timer')?> <a href="<?php echo $timer->getLink() ?>"><?php echo substr($timer->getLink(), 1)?></a></h1>
</div>

<div class="watchers">
  <?php if(count($users) == 0) echo Yii::t('timer', 'no watchers') ?>
  
  <?php foreach($users AS $user):?>
    <span class="watcher"><a href="<?php echo $user->getLink()?>"><?php echo $user->getIdentifier()?></a></span>
  <?php endforeach ?>
</div>