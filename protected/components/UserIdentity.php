<?php

class UserIdentity extends CUserIdentity
{
  private $_id;
  
  public function authenticate()
  {
    $login = strtolower($this->username);
    $user = User::retrieveByLogin($login);
    if($user === null)
      $this->errorCode = self::ERROR_USERNAME_INVALID;
    else if(!$user->validatePassword($this->password))
      $this->errorCode = self::ERROR_PASSWORD_INVALID;
    else
    {
      $this->_id = $user->id;
      $this->errorCode = self::ERROR_NONE;
      $this->username = $user->getAttribute('nickname');
    }
    return $this->errorCode == self::ERROR_NONE;
  }
  
  public function getId()
  {
    return $this->_id;
  }
}