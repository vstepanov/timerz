<?php

class Utility extends CComponent
{
  public static function mb_ucfirst($word)
  { 
    return mb_strtoupper(mb_substr($word, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr(mb_convert_case($word, MB_CASE_LOWER, 'UTF-8'), 1, mb_strlen($word), 'UTF-8');
  }
  
  public static function mb_str_pad($input, $pad_length, $pad_string = ' ', $pad_type = STR_PAD_RIGHT)
  {
    $diff = strlen($input) - mb_strlen($input);
    return str_pad($input, $pad_length+$diff, $pad_string, $pad_type);
  }
  
  public static function nl2br_limit($string, $num)
  { 
    return preg_replace('/\n/', '<br/>', preg_replace('/(\s{'.$num.'})\s+/','$1', $string));
  }
  
  public static function encode_basename($url)
  {
    $url = explode('/', trim($url));
    $base = array_pop($url);
    $part1 = CHtml::encode(implode('/', $url));
    $part2 = rawurlencode($base);
    return $part1 . ($part2 ? '/' . rawurlencode($base) : '');
  }
  
  public static function strtotime($time, /* смещение относительно UTC в секундах, например для Москвы +4*3600 */ $utcOffsetInSeconds)
  {
    try {
      $timezoneName = timezone_name_from_abbr("", $utcOffsetInSeconds, false);
      $dt = new DateTime($time, new DateTimeZone($timezoneName));
      return $dt->getTimestamp();
    } catch(Exception $e) {
      //echo $e->getMessage();
      return false;
    }
  }
}