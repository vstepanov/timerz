<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/main';
  public $page = null; // текущая страница
  public $hasPageTitlePostfix = true;
  
  public function routeIs($controller, $action = null, $actionParams = array())
  {
    $result = Yii::app()->controller->id == $controller;
    if(is_null($action))
      return $result;
    else
      $result = $result && Yii::app()->controller->action->id == $action;
    if(!$result)
      return false;
    $controllerActionParams = Yii::app()->controller->getActionParams();
    foreach($actionParams AS $k => $v)
      $result = $result && isset($controllerActionParams[$k]) && $controllerActionParams[$k] == $v;
    return $result;
  }
  
  protected function checkAuth()
  {
    if(Yii::app()->user->isGuest)
      Yii::app()->user->loginRequired();
  }
}