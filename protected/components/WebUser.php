<?php
 
class WebUser extends CWebUser
{
  public $guestName = '';
  
  public function loginAsUser(User $user)
  {
    $identity = new DummyUserIdentity($user->getPrimaryKey(), $user->getAttribute('nickname'));
    return $this->login($identity);
  }
  
  public function login($identity, $duration = null)
  {
    if(is_null($duration))
      $duration = 3600*24*180;
    return parent::login($identity, $duration);
  }
  
  public function getLink()
  {
    if(!Yii::app()->user->isGuest)
      return User::url(User::identifier(Yii::app()->user->id, Yii::app()->user->name));
  }
}