<?php

class DecodeHashCommand extends Command
{  
  public function actionUser($hash)
  {
    $ret = User::decodeHash($hash);
    $this->writeln(is_null($ret) ? 'null' : $ret);
  }
  
  public function actionTimer($hash)
  {
    $ret = Timer::decodeHash($hash);
    $this->writeln(is_null($ret) ? 'null' : $ret);
  }
}