<?php

class EncodeHashCommand extends Command
{  
  public function actionUser($id)
  {
    $this->writeln(User::encodeHash($id));
  }
  
  public function actionTimer($id)
  {
    $this->writeln(Timer::encodeHash($id));
  }
}