<?php

class CleanCoverTmpCommand extends Command
{

  public function actionIndex()
  {
    $this->writeln('This is cleancovertmp:index');
    $selectSql = 'SELECT hash FROM cover_tmp WHERE created_at + INTERVAL 1 HOUR < NOW()';
    $hashList = CoverTmp::model()->dbConnection->createCommand($selectSql)->queryColumn();
    $hashCount = count($hashList);
    $removedFiles = 0;
    $removedRows = 0;
  	for($i=0;$i<$hashCount;$i++)
  	{
  	  $this->writeln($hashList[$i]);
    	$path = CoverTmp::path($hashList[$i]);
    	if(is_file($path) && unlink($path)) {
    	  $this->writeln('- ' . $path);
    	  $removedFiles++;
      }
  	}
  	if($hashCount > 0)
  	{
  	  $deleteSql = 'DELETE FROM cover_tmp WHERE hash in ("'.implode('","', $hashList).'")';
      $removedRows = CoverTmp::model()->dbConnection->createCommand($deleteSql)->execute();
    }
  	$this->writeln('hash count: ' . $hashCount);
  	$this->writeln('removed files: ' . $removedFiles);
  	$this->writeln('removed rows: ' . $removedRows);
    return 0;
  }
  
}