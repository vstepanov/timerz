<?php

return array(
  'Edit password' => 'Изменить пароль',
  'Set password' => 'Установить пароль',
  'Edit email' => 'Изменить email',
  'Set email' => 'Установить email',
  'Edit nickname' => 'Изменить псевдоним',
  'Set nickname' => 'Установить псевдоним',
  'Edit' => 'Изменить',
  'Set' => 'Установить',
  'defined' => 'установлен',
  'not set' => 'не установлен',
  'at least 4 characters, letters and numbers' => 'не менее 4 символов, английские буквы и цифры',
  'to confirm' => 'для подтверждения операции',
);