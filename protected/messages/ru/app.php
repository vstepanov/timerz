<?php

return array(
  'Timerz - countdown timers' => 'Timerz - таймеры обратного отсчета',
  
  
  'Max file size: {size}mb' => 'Размер файла не должен превышать {size}мб',
  'Upload error, try another' => 'Невозможно загрузить файл, попробуйте другой',
  'Cancel' => 'Отменить',
  'Save' => 'Сохранить',
  'Delete' => 'Удалить',
  'Send' => 'Отправить',
  'Continue' => 'Продолжить',
  'Sign in' => 'Вход',
  'Sign up' => 'Регистрация',
  'Settings' => 'Настройки',
  'Go back' => 'Вернуться назад',
  
  /* Http errors */
  'Access denied' => 'Доступ запрещен',
  'Page not found' => 'Страница не найдена',
  'Error' => 'Ошибка',
  'The page is protected by privacy settings.' => 'Страница, к которой Вы обращаетесь, защищена настройками приватности.',
  'The page does not exist or has been deleted.' => 'Страница, к которой Вы обращаетесь, не существует или была удалена.',
  'This is rarely the case. We have to understand the reasons. Sorry about that!' => 'Такое бывает редко. Мы уже разбираемся в причинах. Приносим свои извинения!',
);