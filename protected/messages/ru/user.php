<?php

return array(
  'Password' => 'Пароль',
  'Nickname' => 'Псевдоним',
  'Current password' => 'Текущий пароль',
  'New password again' => 'Новый пароль снова',
  'New password' => 'Новый пароль',
);