<?php

return array(
  'Sign in' => 'Войти',
  'Sign out' => 'Выйти',
  'New to Timerz?' => 'Еще не зарегистрированы?',
  'Already have account?' => 'Уже зарегистрированы?',
  'Forgot password?' => 'Забыли пароль?',
  'Email or nickname' => 'Email или псевдоним',
  'Wrong login and/or password' => 'Неправильный логин и/или пароль',
  'Restore password' => 'Восстановление пароля',
  'User with this email is already registered' => 'Пользователь с таким email уже зарегистрирован',
  'Invalid email format' => 'Недопустимый формат email',
  'Enter your email or nickname' => 'Введите email или псевдоним',
  'Enter your email' => 'Введите email',
  'Unable to send link to password restore' => 'Не удалось отправить ссылку на восстановление пароля',
  'User not found' => 'Пользователь не найден',
  'Email with instructions to reset your password sent to your email' => 'Сообщение с инструкциями по восстановлению пароля отправлено на Ваш email',
  'Link to the password recovery is not valid' => 'Ссылка на восстановление пароля недействительна',
);