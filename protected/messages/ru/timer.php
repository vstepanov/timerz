<?php

return array(

  /* Show timer */
  '{n} view|{n} views' => '{n} просмотр|{n} просмотра|{n} просмотров|{n} просмотра',
  'Private' => 'Приватный',
  'Edit' => 'Изменить',
  'Created by' => 'Автор',
  'End at' => 'Конец',
  'Views' => 'Просмотры',
  '{n} watcher|{n} watchers' => '{n} подписчик|{n} подписчика|{n} подписчиков|{n} подписчика',
  'Watch' => 'Подписаться',
  'Stop watching' => 'Отписаться',
  'Create countdown timer to your event and share it with family, friends or whole world!' => 'Создайте таймер обратного отсчета до Вашего события и поделитесь им с близкими, друзьями или со всем миром!',
  
  'No timers' => 'Нет таймеров',
  
  /* Menu links */
  'Popular' => 'Популярные',
  'Soon' => 'Скоро',
  'Last' => 'Последние',
  'Ended' => 'Завершились',
  'Watching' => 'Подписки',
  'My timers' => 'Мои таймеры',
  'Home' => 'Главная',
  'New timer' => 'Новый таймер',
  
  'Watchers of timer' => 'Подписчики таймера',
  'no watchers' => 'нет подписчиков',
  
  /* Titles */
  'Last added' => 'Недавно добавленные',
  'Recently ended' => 'Недавно завершились',
  'Popular timers' => 'Популярные таймеры',
  'Coming soon' => 'Скоро завершаются',
  'Edit timer' => 'Изменить таймер',
  'Create timer' => 'Создать таймер',
  
  'Timer ended' => 'Таймер завершился',
  'You can not watch your timer' => 'Вы не можете следить за своим таймером',
  
  /* Homepage promo */
  'Create your own timer' => 'Создайте свой таймер',
  'and share it with friends' => 'и поделитесь им с друзьями',
  'Waiting can be fun' => 'Ожидание может быть увлекательным',
  
  /* Timer privacy */
  'Access for all' => 'Доступен всем',
  'Access from my page' => 'Доступен с моей страницы',
  'Access from user page' => 'Доступен со страницы пользователя',
  'Access whom I will give link' => 'Доступен кому дам ссылку',
  'Access only by link' => 'Доступен только по ссылке',
  'Access only for me' => 'Доступен только мне',
  
  /* Timer form */
  'Caption' => 'Надпись',
  'Message' => 'Сообщение',
  'Expiration' => 'Дата и время события',
  'Comments' => 'Комментарии',
  'Description' => 'Описание',
  'Your email' => 'Ваш email',
  'Privacy' => 'Приватность',
  'Timer must have expiration date' => 'Таймер должен иметь время',
  'Unrecognized format' => 'Неправильный формат',
  'Expiration date should be in the future' => 'Время события должно быть в будущем',
  'Upload picture' => 'Загрузить картинку',
  'Edit picture' => 'Изменить картинку',
  'ex: before my birthday left' => 'например: до моего ДР осталось',
  'ex: {datetime} or {time}' => 'например: {datetime} или {time}',
  'will show after timer expiration' => 'появится после завершения таймера',
  'where to send the link to control' => 'куда отправить ссылку для управления',
  'Additional attributes' => 'Дополнительные параметры',
  'displayed under timer' => 'отображается под таймером',
  'on timer page' => 'на странице таймера',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
);