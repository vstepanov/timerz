<?php

class TimersListWidget extends CWidget
{
  public $criteria;
  public $author = null;
  public $params = array(); // for //timer/show
  
  public function run()
  {
    $timersPerPage = Yii::app()->params['timersPerPage'];
    $actionParams = $this->getController()->getActionParams();
    $page = isset($actionParams['page']) ? (integer) $actionParams['page'] : 1;
    $rowCount = Timer::model()->count($this->criteria);
    $pages = ceil($rowCount/$timersPerPage);
    
    if($page < 1)
      $page = 1;
    elseif($page > $pages)
      $page = $pages;
      
    if(is_null($this->author))
      $this->criteria->with = array('user');
      
    $this->criteria->mergeWith(array('limit' => $timersPerPage, 'offset' => ($page-1)*$timersPerPage));
    
    $timers = Timer::model()->findAll($this->criteria);
    
    $this->params[] = 'thumbnail';
    $this->params[] = 'about-at-bottom';
    $this->render('timers/list', array('timers' => $timers, 'page' => $page, 'pages' => $pages));
  }
}