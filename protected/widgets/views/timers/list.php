<div class="timers">
  <?php if(count($timers) == 0):?>
    <p class="lead notimers"><?php echo Yii::t('timer', 'No timers') ?></p>
  <?php endif ?>
  <?php foreach($timers as $timer):?>
    <?php echo $this->getController()->renderPartial('//timer/show', array('timer' => $timer, 'params' => $this->params, 'author' => is_null($this->author) ? $timer->getAuthor() : $this->author), true)?>
  <?php endforeach ?>

  <?php if(isset($page) && isset($pages) && $pages > 1):?>
    <div class="text-center">
      <ul class="pagination">
        <li<?php if($page == 1):?> class="disabled"<?php endif ?>><a href="<?php echo $page > 1 ? '?page='.($page-1) : '#'?>">&laquo;</a></li>
        <?php for($i=1;$i<=$pages;$i++):?>
          <li<?php if($i == $page):?> class="active"<?php endif ?>>
            <a href="?page=<?php echo $i?>"><?php echo $i?><?php if($i == $page):?> <span class="sr-only">(current)</span><?php endif ?></a>
          </li>
        <?php endfor ?>
        <li<?php if($page == $pages):?> class="disabled"<?php endif ?>><a href="<?php echo $page < $pages ? '?page='.($page+1) : '#'?>">&raquo;</a></li>
      </ul>
    </div>
  <?php endif ?>
</div>