function leadZero(num) {
  return num < 10 ? '0'+num : num;
}

//-----------------------------------------------

function debounce(func, wait, immediate) {
  var result;
  var timeout = null;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) result = func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) result = func.apply(context, args);
    return result;
  };
};

//-----------------------------------------------

function loadImage(src, onSuccessCallback, onFailCallback) {
  var loaded = false,
      img = new Image;
  function loadHandler() {
    if (loaded)
      return;
    loaded = true;
    if(typeof onSuccessCallback == 'function')
      onSuccessCallback.call(this);
  }
  img.onload = loadHandler;
  if(typeof onFailCallback == 'function') {
    img.onerror = onFailCallback;
    img.onabort = img.onerror;
  }
  img.src = src;
  if (img.complete)
    loadHandler.call(img);
  return img;
}