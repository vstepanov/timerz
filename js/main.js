var maintimer = null, timers = {};

//===============================================

(function() {
  
  var
    dynamicCoverImg = null,
    menuShow = null;
  
  //-----------------------------------------------
  
  window.setDynamicCover = function(src, callbackSuccess, callbackFail) {//console.log('setDynamicCover');
    function doSets() {
      setDynamicCoverHeight();
      setLightFooter();
      setCompactNavbar();
      if(maintimer && maintimer.isCentered)
        setMaintimerOnCenter();
      if(typeof callbackSuccess == 'function')
          callbackSuccess();
    }
    if(typeof src == 'undefined')
      doSets();
    else {
      var img = loadImage(src, function() {
        $('#dynamic-cover').css('background-image', 'url('+this.src+')');
        $('body').addClass('covered');
        $('#dynamic-cover').show();
        dynamicCoverImg = this;
        doSets();
      }, function() {
        dynamicCoverImg = null;
        if(typeof callbackFail == 'function')
          callbackFail();
      });
    }
    return true;
  }
  
  //-----------------------------------------------
  
  function unsetDynamicCover() {
    $('#dynamic-cover').hide();
    dynamicCoverImg = null;
    $('body').removeClass('covered');
    $('#dynamic-cover').css('background-image', 'none');
    setCompactNavbar();
  }
  
  //-----------------------------------------------
  
  function setTimerCover(cover_url, timer, callback) {//console.log('setTimerCover('+cover_url+', '+(typeof timer)+')');
    if(typeof timer == 'undefined') {
      if(maintimer)
        timer = maintimer;
      else
        return;
    }
    if(maintimer && timer == maintimer && maintimer.useDynamicCover)
      setDynamicCover(cover_url, callback, callback);
    else {
      if(typeof callback == 'function')
        callback();
      loadImage(cover_url, function() {
        timer.getContainer().css('background-image', 'url('+this.src+')');
        timer.getContainer().parent().addClass('covered');
      });
    }
  }
  
  //-----------------------------------------------
  
  function unsetTimerCover(timer) {
    if(typeof timer == 'undefined') {
      if(maintimer)
        timer = maintimer;
      else
        return;
    }
    if(maintimer && timer == maintimer && maintimer.useDynamicCover)
      unsetDynamicCover();
    else {
      timer.getContainer().css('background-image', 'none');
      timer.getContainer().parent().removeClass('covered');
    }
  }
  
  //-----------------------------------------------
  
  function setDynamicCoverHeight() {//console.log('setDynamicCoverHeight');
    if(!dynamicCoverImg) {//console.log('!dynamicCoverImg');
      return;
    }
    var
      $dynamicCover           = $('#dynamic-cover'),
      viewportWidth           = $(window).width(),
      viewportHeight          = $(window).height(),
      aspectRatio             = dynamicCoverImg.width / dynamicCoverImg.height,
      minHeight               = 500,
      maxHeight               = $(document).height(),
      dynamicCoverHeight      = Math.round(viewportWidth / aspectRatio),
      $undertimer             = $('.dynamic-under'),
      newDynamicCoverHeight   = dynamicCoverHeight;

    if (dynamicCoverHeight <= maxHeight) {
      if (dynamicCoverHeight >= minHeight)
        newDynamicCoverHeight = dynamicCoverHeight;
      else
        newDynamicCoverHeight = minHeight;
    }
    else
      newDynamicCoverHeight = maxHeight;
    $dynamicCover.height(newDynamicCoverHeight);
    
    if($undertimer.length != 0 && maintimer) {
      var
        maintimerOffsetTop      = $('.dynamic-bottom-border').offset().top,
        maintimerHeight         = $('.dynamic-bottom-border').height(),
        maintimerBottom         = maintimerOffsetTop + maintimerHeight,
        undertimerOffsetTop     = $undertimer.offset().top,
        undertimerNewOffsetTop  = Math.max(newDynamicCoverHeight, maintimerBottom) - maintimerBottom;
      //console.log('maintimerBottom = [' + maintimerBottom + '] newDynamicCoverHeight = ['+newDynamicCoverHeight+']');
      $undertimer.css('margin-top', undertimerNewOffsetTop + 'px');
    }
  }
  
  //-----------------------------------------------
  
  function setLightFooter() {//console.log('setLightFooter');
    if(!dynamicCoverImg)
      return;
    if($('#footer').offset().top < $('#dynamic-cover').height())
      $('#footer').addClass('light');
    else
      $('#footer').removeClass('light');
  }
  
  //-----------------------------------------------
  
  function setMaintimerOnCenter() {//console.log('setMaintimerOnCenter');
    var
      viewportHeight          = $(window).height(),
      viewportWidth           = $(window).width(),
      dynamicCoverHeight      = $('#dynamic-cover').height(),
      maintimerOffsetTop      = $('.maintimer').offset().top,
      $centeredElement        = $('.maintimer .cmt-container'),
      containerPaddingTop     = parseInt($('#content > .container').css('padding-top')),
      tickOffsetTop           = $centeredElement.offset().top,
      tickHeight              = $centeredElement.height(),
      tickCenter              = tickOffsetTop + tickHeight/2,
      maintimerNewOffsetTop   = maintimerOffsetTop + (dynamicCoverHeight > 0 ? Math.min(viewportHeight, dynamicCoverHeight) : viewportHeight)/2 - tickCenter - 40 - containerPaddingTop/2; // 40 is .tick margin top and 20 is about half navbar height 
      
    if(viewportWidth < 480 && !dynamicCoverImg)
      maintimerNewOffsetTop = 20;
    
    if(maintimerNewOffsetTop > 0)
      $('.maintimer').css('margin-top', maintimerNewOffsetTop + 'px');
  }
  
  //-----------------------------------------------
  
  function setCompactNavbar() {
    var $timerzNav = $('#timerz-nav');
    if(dynamicCoverImg || $(document).scrollTop() > 30 || $(window).width() < 992) {
      if(!$timerzNav.hasClass('compact'))
        ticklogoTransition();
      $timerzNav.addClass('compact');
      $('.ticklogo').attr('href', '#');
      if(!menuShow)
        $timerzNav.find('.menu').hide();
      return true;
    }
    else {
      $('.ticklogo').attr('href', '/');
      $timerzNav.find('.menu').show();
      $timerzNav.removeClass('compact');
      return false;
    }
  }
  
  //-----------------------------------------------
  
  function ticklogoTransition(delay, scale) {
    if(typeof scale == 'undefined')
      scale = 1.2;
    if(typeof delay == 'undefined')
      delay = 0;
    $('.ticklogo').transition({ scale: scale, delay: delay }, 'fast', function() {$('.ticklogo').transition({scale: 1})});  
  }

  //=============================================== 
  
  function runMaintimerOnCenter() {//console.log('runMaintimerOnCenter');
    if(maintimer && maintimer.isCentered) {
      $(window).resize(debounce(setMaintimerOnCenter, 50));
      setMaintimerOnCenter();
    }
  }
  
  //-----------------------------------------------
  
  function runMaintimerTickTitle() {//console.log('runMaintimerTickTitle');
    if($('body').hasClass('timerpage') && maintimer && !maintimer.isExpired()) {
      var titlePostfix = document.title;
      timerz.onTick(function() {
        var title = '', timeleft = maintimer.getTimeleft();
        if(timeleft == 0) {
          title = "Таймер завершился";
        } else {
          if(timeleft > 86400)
            title += maintimer.timeleft.d + 'д';
          if(timeleft > 3600)
            title += (title ? ' ': '') + leadZero(maintimer.timeleft.h);
          if(timeleft > 60)
            title += (title ? ':': '') + leadZero(maintimer.timeleft.m);
          title += (title ? ':': '') + leadZero(maintimer.timeleft.s);
        }
        document.title = title + ' - ' + titlePostfix;
      });
    }
  }
  
  //-----------------------------------------------
  
  function runLightFooter() {//console.log('runLightFooter');
    $(window).resize(debounce(setLightFooter, 50));
  }
  
  //-----------------------------------------------
  
  function runDynamicCover() {//console.log('runDynamicCover');
    $(window).resize(debounce(setDynamicCoverHeight, 50));
    setDynamicCoverHeight();
  }
  
  //-----------------------------------------------
  
  function runTicklogo() {//console.log('runTicklogo');
    timerz.onTick(function(){
      $('.ticklogo span').addClass('trans');
      setTimeout(function() {
        $('.ticklogo span').removeClass('trans');
      }, 500);
      /*$('.ticklogo span').fadeOut(500, function() {
        $('.ticklogo span').fadeIn(500);
      });*/
    });
  }
  
  //-----------------------------------------------
  
  function runCompactNavbar() {//console.log('runCompactNavbar');
    var timeoutId = null;
    $(window).resize(debounce(setCompactNavbar, 50));
    $(window).scroll(setCompactNavbar);
    
    setTimeout(function() {
    $(document).on('mouseover', '#timerz-nav.compact .ticklogo, #timerz-nav.compact .menu', function(event) {
      $('#timerz-nav .menu').show();
      if(timeoutId)
        clearTimeout();
      menuShow = true;
    });}, 100);
    
    $(document).on('mouseout', '#timerz-nav.compact .menu, #timerz-nav.compact .ticklogo', function(event) {
      menuShow = false;
      if(!timeoutId) {
        timeoutId = setTimeout(function() {
          if(menuShow == false)
            $('#timerz-nav .menu').hide();
          timeoutId = null;
        }, 300);
      }
    });
    $(document).on('click', '#timerz-nav.compact .ticklogo', function(event) {
      menuShow = !menuShow;
      if(menuShow)
        $('#timerz-nav .menu').show();
        if(timeoutId)
          clearTimeout();
    });
    setCompactNavbar();
  }
  
  //-----------------------------------------------
  
  function runTimerForm() {//console.log('runTimerForm');
    var
    isSubmited = false,
    fields = ['caption', 'expires', 'email', 'message', 'description'],
    onChangeAtWork = false,
    onChangeCallback = null,
    validator = {
      caption: function(callback) {
        var $controlElement = $('#timer_caption'), caption = $.trim($controlElement.val());
        if(caption.length == 0)
          return error($controlElement, 'У таймера должна быть надпись', callback);
        else if(caption.length > 255)
          return error($controlElement, 'Длина должна быть < 300 символов', callback);
        else {
          if(maintimer) {
            maintimer.renderer().init();
            maintimer.renderer().renderCaption(caption);
          }
          return success($controlElement, callback);
        }
      },
      email: function (callback) {
        var 
          $controlElement = $('#timer_email'),
          email = $.trim($controlElement.val());
          atpos = email.indexOf("@"),
          dotpos = email.lastIndexOf(".");
        if($controlElement.length == 0)
          return success($controlElement, callback);
        else if(email.length == 0)
          return error($controlElement, 'Укажите email', callback);
        else if(email.length > 45)
          return error($controlElement, 'Длина должна быть < 45 символов', callback);
        else if(atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
          return error($controlElement, 'Неправильный формат', callback);
        else
          return success($controlElement, callback);
      },
      expires: function (callback) {
        var $controlElement = $('#timer_expires'), expires = $.trim($controlElement.val());
        if(expires.length == 0)
          return error($controlElement, 'Когда таймер должен сработать?', callback);
        $.get('/timer/strtotime', {time: expires, utcoffset: (new Date()).getTimezoneOffset()}, function(resp) {
          var timestamp = parseInt(resp);
          if(!timestamp)
            return error($controlElement, 'Не могу распознать, попробуйте формат дд.мм.гггг чч:мм:сс', callback);
          else if(timestamp <= +new Date()/1000|0)
            return error($controlElement, 'Таймер не может сработать в прошлом', callback);
          else {
            if(maintimer) {
              maintimer.renderer().init(true);
              maintimer.setExpires(timestamp);
            }
            return success($controlElement, callback);
          }
        });
      },
      message: function (callback) {
        var $controlElement = $('#timer_message'), message = $.trim($controlElement.val());
        if(message.length > 1000)
          return error($controlElement, 'Длина должна быть < 1000 символов', callback);
        else {
          if(maintimer)
            maintimer.setMessage(message);
          return success($controlElement, callback);
        }
      },
      description: function (callback) {
        var $controlElement = $('#timer_description'), description = $.trim($controlElement.val());
        if(description.length > 1000)
          return error($controlElement, 'Длина должна быть < 1000 символов', callback);
        else {
          /*if(maintimer) {
            maintimer.renderer().init();
            maintimer.renderer().renderDescription(description);
          }*/
          return success($controlElement, callback);
        }
      },
    };
    
    function error($controlElement, message, callback) {
      $controlElement.parent().removeClass('has-success').addClass('has-error');
      $controlElement.parent().find('.error').html(message);
      if(typeof callback == 'function')
        callback(false);
    }

    function success($controlElement, callback) {
      $controlElement.parent().removeClass('has-error').addClass('has-success');
      if(typeof callback == 'function')
        callback(true);
    }

    function validate(callback) {
      var isValid = true, validatorCallsRemain = fields.length;
      function valid(valided) {
        isValid = isValid && valided;
        validatorCallsRemain--;
        if(validatorCallsRemain == 0)
          callback(isValid);
      }
      for(var i=0;i<fields.length;i++) {
        var fieldName = fields[i];
        if(onChangeAtWork == fieldName)
          onChangeCallback = valid;
        else
          validator[fieldName](valid);
      }
    }
    
    function submit() {
      if(!isSubmited)
      {
        submited();
        validate(function(isValid) {
          if(isValid) {
            var timerData = {};
            $('.timer-form .value').each(function(index) {
              var
                $controlElement = $(this),
                name            = $controlElement.attr('name'),
                type            = $controlElement.attr('type'),
                value           = $controlElement.val();
              if(!(type == 'checkbox' && !$controlElement.prop('checked')))
                timerData[name] = value;
            });
            //console.log(timerData);
            $.post($('.timer-form').attr('action'), timerData, function(response) {
              if(typeof response.result != 'undefined' && response.result == 'ok' && typeof response.url)
                window.location.href = response.url;
              else {
                resetSubmited();
                if(typeof response.errors != 'undefined') {
                  var s = '';
                  for(var i in response.errors)
                    s = (s ? "\r\n" : '') + response.errors[i];
                  alert(s);
                }
                else
                  alert('Ошибка (-1)');
                //console.log(response);
              }
            }, 'json').fail(function() {resetSubmited();alert('Произошел сбой, попробуйте еще раз');});
          }
          else
            resetSubmited();
        });
      }
      return false;
    }
    
    function submited() {
      isSubmited = true;
      $('.timer-form button[type="submit"]').button('loading');
    }
    
    function resetSubmited() {
      isSubmited = false;
      $('.timer-form button[type="submit"]').button('reset');
    }
    
    function change() {
      var controlName = $(this).attr('name'),
            fieldName = controlName.substring(6, controlName.length - 1);
      onChangeAtWork = fieldName;
      setTimeout(function() {
        validator[fieldName](function(isValid) {
          onChangeAtWork = false;
          if(typeof onChangeCallback == 'function') {
            onChangeCallback(isValid);
            onChangeCallback = null;
          }
        });
      }, 50);
      return true;
    }
    
    function showAdditional() {
      $('.timer-form .additional-show').hide();
      $('.timer-form .additional-hide').show();
      $('.timer-form .additional').show();
      return false;
    }
    
    function hideAdditional() {
      $('.timer-form .additional-show').show();
      $('.timer-form .additional-hide').hide();
      $('.timer-form .additional').hide();
      return false
    }
    
    var expiresTimestamp = $('#timer_expires').data('timestamp');
    if(expiresTimestamp)
      $('#timer_expires').val((new Date(expiresTimestamp*1000)).format(expiresTimestamp%60 ? 'dd.mm.yyyy HH:MM:ss' : 'dd.mm.yyyy HH:MM'));
    
    $('#timer_caption').bind('change', change);
    $('#timer_expires').bind('change', change);
    $('#timer_email').bind('change', change);
    $('#timer_message').bind('change', change);
    $('#timer_description').bind('change', change);
    
    $('.timer-form .additional-show').bind('click', showAdditional);
    $('.timer-form .additional-hide').bind('click', hideAdditional);
    
    $('.timer-form').bind('submit', submit);
    
    $(document).on('click', '.timer-form .cover .delete', function() {
      $('.timer-form').removeClass('has_cover');
      unsetTimerCover();
      $('#timer_cover').val('delete');
    });
    
    function coverUploadFinish() {
      resetSubmited();
      $('.timer-form .progress').hide();
    }
    
    $('#timer_cover_upload').fileupload({
      dataType: 'json',
      add: function (e, data) {
        //console.log('add');
        $('.timer-form .progress-bar').css('width','0%');
        $('.timer-form .progress').show();
        submited();
        data.submit();
      },
      done: function (e, data) {
        //console.log('done');console.log(data.result);
        coverUploadFinish();
        if(!data.result.error) {
          $('#timer_cover').val(data.result.coverTmpHash);
          setTimerCover(data.result.coverUrl);
          $('.timer-form').addClass('has_cover');
        } else alert(data.result.errorMessage);
      },
      fail: function(e, data) {
        coverUploadFinish();
        alert('Невозможно загрузить файл, попробуйте другой');
      },
      progressall: function (e, data) {
        //console.log('progress');
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('.timer-form .progress-bar').css('width',progress + '%');
      }
    });
  }
  
  //-----------------------------------------------
  
  function runTimers(callback) {//console.log('runTimers');
    var callsRemain = $('.timer-container').length;
    if(callsRemain == 0) {
      if(typeof callback == 'function')
        callback();
      return;
    }
    function callbackCall() {
      callsRemain--;
      if(callsRemain == 0 && typeof callback == 'function')
        callback();
    }
    $('.timer-container').each(function(index) {
      var
        $timerContainer = $(this),
        $timer          = $timerContainer.find('.timer'),
        id              = $timer.attr('id').substring(5),
        expires         = $timer.data('expires'),
        cover_url       = $timer.data('cover-url'),
        isMaintimer     = $timer.hasClass('maintimer'),
        useDynamicCover = $timer.data('dynamic-cover'),
        isCentered      = $timer.data('centered'),
        timer           = new timerz(id, expires);
      timers[id] = timer;
      if(isMaintimer) {
        maintimer = timer;
        maintimer.isCentered = isCentered;
        maintimer.useDynamicCover = useDynamicCover;
        maintimer.onExpire(function() {
          setDynamicCoverHeight();
          setMaintimerOnCenter();
        });
      }
      if(!id) {
        if(!expires)
          timer.setExpires(-1 + 31536000+new Date()/1000|0);
        if(!timer.renderer().getCaption())
          timer.renderer().renderCaption('[Надпись еще не установлена]');
      }
      if(cover_url) {//console.log('cover_url = ['+cover_url+']');
        setTimerCover(cover_url, timers[id], callbackCall);
      }
      else
        callbackCall();
    });
  }
  
  //-----------------------------------------------
  
  function runTimestampReplace() {
    $('.timestamp-replace').each(function() {
      var timestamp = $(this).data('timestamp');
      $(this).html((new Date(timestamp*1000)).format(timestamp%60 ? 'dd.mm.yyyy HH:MM:ss' : 'dd.mm.yyyy HH:MM'));
    });
  }
  
  //-----------------------------------------------
  
  function runTimersClick() {
    $(document).on('click', '.timers .has_timerlink .timer', function() {
      window.location.href = $(this).find('.timerlink').attr('href');
    });
  }
  
  //-----------------------------------------------
  
  function runSetScreenWidthInCookie() {
    if(!getCookie('screen_width'))
      setCookie('screen_width', screen.width, {path: '/'});
  }
  
  //-----------------------------------------------
  
  function runFlipMaintimer() {
    if($('body').hasClass('timerpage') && maintimer) {
      maintimer.getContainer().find('.cmt-container').click(function() {
        if(maintimer.isExpired()) {
          if(maintimer.getContainer().find('.caption').hasClass('hide')) {
            maintimer.getContainer().find('.caption').removeClass('hide');
            maintimer.getContainer().find('.tick').removeClass('hide');
            maintimer.getContainer().find('.message').addClass('hide');
          } else {
            maintimer.getContainer().find('.caption').addClass('hide');
            maintimer.getContainer().find('.tick').addClass('hide');
            maintimer.getContainer().find('.message').removeClass('hide');
          }
          setMaintimerOnCenter();
        }
      });
    }
  }
  
  //-----------------------------------------------
  
  $(document).ready(function() {
    runTicklogo();
    runTimerForm();
    runTimers(function() {
      //console.log('timers loaded');
      runMaintimerOnCenter();
      runDynamicCover();
      runCompactNavbar();
      runLightFooter();
      runMaintimerTickTitle();
      runFlipMaintimer();
    });
    runTimestampReplace();
    runTimersClick();
    runSetScreenWidthInCookie();
  });
  
})();