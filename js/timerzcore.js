;(function(window) {
  var _timers = {}, // таблица таймеров
  _id = 0, // последний id
  _ticks = [], // список id тикающих таймеров
  _tickListeners = [], // список подписчиков на событие tick
  _time = +new Date()/1000|0, // текущий timestamp
  
  timerz = function(realid, expires) {
    var id = ++_id;
    this.id = id;
    this.timeleft = {d: null, h: null, m: null, s: null};
    _timers[id] = {id: id, object: this, expires: null, timeleft: null, realid: realid, container: $('#timer'+realid), message: null, onexpire: null};
    if(typeof expires != 'undefined')
      _setExpires(id, expires);
  }
  
  window.timerz = timerz;
  
  timerz.onTick = function(tick) {
    if(typeof tick == 'function')
      _tickListeners.push(tick);
  }
  
  timerz.prototype.setMessage = function(message) {
    _timers[this.id].message = message;
  }
  
  timerz.prototype.onExpire = function(onexpire) {
    if(typeof onexpire == 'function')
      _timers[this.id].onexpire = onexpire;
  }
  
  
  timerz.prototype.isExpired = function() {
    return _isExpired(this.id);
  }
  
  timerz.prototype.setExpires = function(expires) {
    _setExpires(this.id, expires);
  }
  
  timerz.prototype.getExpires = function() {
    return _timers[this.id].expires;
  }
  
  timerz.prototype.getTimeleft = function() {
    return _timers[this.id].timeleft;
  }
  
  timerz.prototype.getContainer = function() {
    return _timers[this.id].container;
  }
  
  function _setExpires(id, expires) {
    var _expires = null;
    if(typeof expires == 'object' && expires instanceof Date)
      _expires = expires.getTime()/1000|0; // |0 - отбрасывает дробную часть
    else
      _expires = expires;
    _timers[id].expires = parseInt(_expires);
    _checkTick(id);
  }
  
  function _isExpired(id) {
    return _timers[id].expires < +new Date()/1000|0;
  }
  
  function _checkTick(id) {
    var timer = _timers[id];
    if(!_isExpired(id)) {
      timer.object.renderer().init();
      if(jQuery.inArray(id, _ticks) < 0)
        _ticks.push(id);
      _tickOnTimer(id);
    }
  }
  
  function _tickOnTimer(id) {
    var timer = _timers[id];
    timer.timeleft = timer.expires - _time;
    //console.log('timeleft: ' + timer.timeleft);
    if(timer.timeleft <= 0) {
      timer.timeleft = 0;
      _ticks.splice(jQuery.inArray(timer.id, _ticks), 1); // удаляем id-таймера из списка тикающих
      timer.container.addClass('expired');
      if(timer.message != null) {
        timer.object.renderer().renderMessage(timer.message);
        if(timer.onexpire != null)
          timer.onexpire();
      } else if(timer.object.renderer().getMessage()) {
        timer.object.renderer().renderMessage();
        if(timer.onexpire != null)
          timer.onexpire();
      } else {
        $.get('/timer/getMessage', {id: timer.realid}, function(message) {
          timer.message = message;
          timer.object.renderer().renderMessage(message);
          if(timer.onexpire != null)
            timer.onexpire();
        }).fail(function() { alert('Error timer get message'); });
      }
    }
    timer.object.timeleft.d = timer.timeleft/86400|0;
    var t = timer.object.timeleft.d*86400;
    timer.object.timeleft.h = (timer.timeleft - t)/3600|0;
    t += timer.object.timeleft.h*3600;
    timer.object.timeleft.m = (timer.timeleft - t)/60|0;
    timer.object.timeleft.s = timer.timeleft - t - timer.object.timeleft.m*60;
    timer.object.renderer().render();
  }

  function _tick() {
    _time = +new Date()/1000|0;
    for(var i=0;i<_ticks.length;i++)
      _tickOnTimer(_ticks[i]);
    for(var i=0;i<_tickListeners.length;i++)
      _tickListeners[i]();
    setTimeout(_tick, 1000);
  }
  
  _tick();
  
})(window);