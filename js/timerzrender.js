timerz.prototype.renderer = function() {
  var timer = this;
  return {
    init: function(force) {
      if(typeof force == 'undefined')
        force = false;
      var $container = timer.getContainer();
      $container.find('.message').addClass('hide');
      $container.find('.tick').removeClass('hide');
      $container.find('.caption').removeClass('hide');
      $container.find('.d').removeClass(force ? 'hide' : '').show();
      $container.find('.h').removeClass(force ? 'hide' : '').show();
      $container.find('.m').removeClass(force ? 'hide' : '').show();
    },
    render: function() {
      var
        timeleft = timer.getTimeleft(),
        $container = timer.getContainer();
      if(timeleft < 86400)
        $container.find('.d').hide();
      else {
        $container.find('.d .count').html(timer.timeleft.d);
        var n1 = parseInt((''+timer.timeleft.d).slice(-1)), n2 = parseInt((''+timer.timeleft.d).slice(-2));
        $container.find('.d .name').html(n2 > 10 && n2 < 20 || n1 >= 5 || n1 == 0 ? 'дней' : (n1 == 1 ? 'день' : 'дня'));
      }
      
      if(timeleft < 3600)
        $container.find('.h').hide();
      else
        $container.find('.h').html(leadZero(timer.timeleft.h));
        
      if(timeleft < 60)
        $container.find('.m').hide();
      else
        $container.find('.m').html(leadZero(timer.timeleft.m));
        
      $container.find('.s').html(leadZero(timer.timeleft.s));
    },
    renderMessage: function(message) {
      var $container = timer.getContainer();
      $container.find('.message').html(typeof message == 'undefined' ? this.getMessage() : message);
      $container.find('.message').removeClass('hide');
      $container.find('.tick').addClass('hide');
      $container.find('.caption').addClass('hide');
      //$container.find('.description').addClass('hide');
    },
    getMessage: function() {
      return $.trim(timer.getContainer().find('.message').html());
    },
    renderCaption: function(caption) {
      var $container = timer.getContainer();
      $container.find('.caption').html(typeof caption == 'undefined' ? this.getCaption() : caption);
      $container.find('.caption').removeClass('hide');
    },
    getCaption: function() {
      return $.trim(timer.getContainer().find('.caption').html());
    },
    renderDescription: function(description) {
      var $container = timer.getContainer();
      if(description) {
        $container.find('.description').html(typeof description == 'undefined' ? this.getDescription() : description);
        $container.find('.description').removeClass('hide');
      } else $container.find('.description').addClass('hide');
    },
    getDescription: function() {
      return $.trim(timer.getContainer().find('.description').html());
    }
  };
};
